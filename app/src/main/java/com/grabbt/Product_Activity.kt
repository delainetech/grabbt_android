package com.grabbt

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.Producttems_Adapter
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.Product
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import kotlinx.android.synthetic.main.productlist_activity.*
import org.json.JSONObject
import java.util.HashMap

class Product_Activity : AppCompatActivity(),View.OnClickListener ,ParamKeys{


    private lateinit var linearLayoutManager:GridLayoutManager

   lateinit var  context : Context
    val list: ArrayList<Product> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.productlist_activity)
        context=this
        initil()
    }

    private fun initil() {


        linearLayoutManager = GridLayoutManager(context,2)
        recycler_explore.layoutManager = linearLayoutManager
        recycler_explore.adapter= Producttems_Adapter(context,list)

        ivback.setOnClickListener(this)

        product_listing(recycler_explore,context)
    }

    override fun onClick(p0: View?) {
        finish()
     }

    fun product_listing(view: View,context: Context){


        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager,"")

        val stringRequest = object : StringRequest(
           Method.POST,baseurl+"product_list", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("closing_compaigns",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                        val jsobj=jsonArray.getJSONObject(i)

                        list.add( Product(Common.ParseString(jsobj, "id"),
                            Common.ParseString(jsobj, "title"),
                            Common.ParseString(jsobj, "discription"),
                            Common.ParseString(jsobj, "image"),
                            Common.ParseString(jsobj, "status")))
                    }

                    recycler_explore.adapter!!.notifyDataSetChanged()

                }else if (obj.getString("status").equals("false") && obj.getString("message").equals("Nothing Found!!")){
//                    view!!.tvcampain.visibility =View.INVISIBLE
//                    recycler_closing.visibility =View.GONE

                }
                else
                    if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()


                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

}