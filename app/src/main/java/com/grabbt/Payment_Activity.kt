package com.grabbt

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import android.webkit.WebView
import kotlinx.android.synthetic.main.payment_activity.*
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import org.json.JSONObject
import java.util.HashMap


class Payment_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys {
    override fun onClick(p0: View?) {
        finish()
    }

    lateinit var prefrences : UserSharedPrefrences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_activity)
        initil()
    }

    private fun initil() {

        prefrences = UserSharedPrefrences.getInstance(this)
        webview.getSettings().javaScriptEnabled = true
         webview.getSettings().loadWithOverviewMode = true


        ivback.setOnClickListener(this)
        webview.setWebViewClient(object : WebViewClient() {


            override fun onPageFinished(view: WebView, url: String) {
                // do your stuff here
                loggg("url",url)
                if (url.equals("https://grabbtapp.com/payment/success.html")){
                    checkout()
                }
            }
        })

        webview.loadUrl("https://grabbtapp.com/payment/index.php?name="+prefrences.getname()+prefrences.getlname()
                +"&email="+prefrences.getemail()+ "&amount="+intent.getStringExtra("price")+"&phone="+prefrences.getmobile()
                +"&order_id="+intent.getStringExtra("orderid"))


    }

    fun checkout(){


        startActivity(
            Intent(this,SuccessfullsubmitActivity::class.java)
                .putExtra("order_id",intent.getStringExtra("order_id")))

//        val pd= DelayedProgressDialog()
//
//        pd.show(supportFragmentManager, "")
//        val stringRequest = object : StringRequest(
//            Request.Method.POST,baseurl+"checkout", Response.Listener<String>
//            {
//                    response ->
//
//                Common.loggg("checkout",response);
//
//                val obj= JSONObject(response)
//                if (obj.getString("status").equals("true")){
//
//
//
//
////                    Common.Snakbar("Order has been placed",view!!);
//                }else if (obj.getString("status").equals("false")){
//                    Common.Snakbar(obj.getString("message") ,ivback!!);
//                }
//                pd.cancel()
//
//            },
//            Response.ErrorListener {
//                    error ->
//                pd.cancel()
//                Log.e("error","",error);
//                Common.Snakbar( Common.volleyerror(error),ivback!!);
//
//            })
//        {
//            override fun getParams(): MutableMap<String, String> {
//                val param = HashMap<String, String>()
//                param["user_id"] = prefrences.getuserid()
//                param["order_details"] =intent.getStringExtra("jsonArray")
//                param["earned"] = intent.getStringExtra("earned")
//                param["spend"] ="0"
//                param["price"] =intent.getStringExtra("price")
//                loggg("params",param.toString())
//                return param
//            }
//        }
//
//        val socketTimeout = 50000
//        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
//        stringRequest.retryPolicy = policy
//
//
//        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }
}