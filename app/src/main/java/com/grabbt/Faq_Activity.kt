package com.grabbt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.grabbt.Adapters.Faq_Adapter
import com.grabbt.Adapters.Tickettems_Adapter
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import kotlinx.android.synthetic.main.faq_activity.*
import kotlinx.android.synthetic.main.wishlist_fragment.view.*

class Faq_Activity : AppCompatActivity() ,View.OnClickListener {
    override fun onClick(p0: View?) {
        finish()
    }

    lateinit var recyclerview: RecyclerView
    private lateinit var linearLayoutManager:LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.faq_activity)
        initil()
    }

    private fun initil() {

        recyclerview=rvfaq;
        linearLayoutManager = LinearLayoutManager(this)
        recyclerview.layoutManager = linearLayoutManager
        recyclerview.adapter= Faq_Adapter(this)
        recyclerview.isNestedScrollingEnabled = false

        ivback.setOnClickListener(this)


    }
}