package com.grabbt.Models;

public class Tickets {

    String order_id, id,product_title,product_disc,product_spec,product_image
            ,price,quantity,prize_title,prize_spec,prize_image,prize_video
            ,prize_disc,tickets,order_count,timestamp;

    public Tickets(String order_id, String id, String product_title, String product_disc, String product_spec, String product_image
            , String price,  String quantity, String prize_title, String prize_spec, String prize_image, String prize_video,
                   String prize_disc, String tickets, String order_count, String timestamp) {
        this.order_id = order_id;
        this.id = id;
        this.product_title = product_title;
        this.product_disc = product_disc;
        this.product_spec = product_spec;
        this.product_image = product_image;
        this.price = price;
        this.quantity = quantity;
        this.prize_title = prize_title;
        this.prize_spec = prize_spec;
        this.prize_image = prize_image;
        this.prize_video = prize_video;
        this.prize_disc = prize_disc;
        this.tickets = tickets;
        this.order_count = order_count;
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getOrder_count() {
        return order_count;
    }

    public void setOrder_count(String order_count) {
        this.order_count = order_count;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getProduct_disc() {
        return product_disc;
    }

    public void setProduct_disc(String product_disc) {
        this.product_disc = product_disc;
    }

    public String getProduct_spec() {
        return product_spec;
    }

    public void setProduct_spec(String product_spec) {
        this.product_spec = product_spec;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrize_title() {
        return prize_title;
    }

    public void setPrize_title(String prize_title) {
        this.prize_title = prize_title;
    }

    public String getPrize_spec() {
        return prize_spec;
    }

    public void setPrize_spec(String prize_spec) {
        this.prize_spec = prize_spec;
    }

    public String getPrize_image() {
        return prize_image;
    }

    public void setPrize_image(String prize_image) {
        this.prize_image = prize_image;
    }

    public String getPrize_video() {
        return prize_video;
    }

    public void setPrize_video(String prize_video) {
        this.prize_video = prize_video;
    }

    public String getPrize_disc() {
        return prize_disc;
    }

    public void setPrize_disc(String prize_disc) {
        this.prize_disc = prize_disc;
    }

    public String getTickets() {
        return tickets;
    }

    public void setTickets(String tickets) {
        this.tickets = tickets;
    }
}
