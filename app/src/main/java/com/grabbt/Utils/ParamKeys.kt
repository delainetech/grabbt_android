package com.grabbt.Utils

interface ParamKeys {


    //    Live server
    val baseurl: String   get() = "https://grabbtapp.com/api/"
 val imageurl : String   get() = "https://grabbtapp.com/images/"
    val Id : String   get() = "id"
   val F_name : String   get() = "f_name"
   val L_name : String   get() = "l_name"
     val Gender : String   get() = "gender"
    val Email : String   get() = "email"
     val Phone : String   get() = "phone"



    val weburl: String   get() = "http://134.209.86.177/app-admin/public/"


    val Googlemap_imgurl : String   get() =     "http://maps.google.com/maps/api/staticmap?key=AIzaSyBXAdCe4nJuapECudMeh4q-gGlU-yAMQX0&zoom=13&size=500x500&sensor=false&markers=color:red|"
    val Password : String   get() = "password"
    val Name : String   get() = "name"
    val Profile_image : String   get() = "profile_image"
    val Addresss : String   get() = "address"
    val Token : String   get() = "token"
    val Social_Details : String   get() = "social_details"
    val DOB : String   get() = "DOB"
    val About : String   get() = "about"
    val Player_id : String   get() = "player_id"

    val Ad_title : String   get() = "ad_title"


    val Type : String   get() = "type"
    val OTP : String   get() = "otp"
    val Mobile : String   get() = "phone"
    val Ad_description : String   get() = "ad_description"
    val Price : String   get() = "price"
    val Images : String   get() = "image"
    val Category : String   get() = "category"
    val City : String   get() = "city"
    val Ttimestamp : String   get() = "timestamp"
    val Bid_type : String   get() = "bid_type"
    val Company_Info : String   get() = "company_info"
    val Bid_price : String   get() = "bid_price"
    val User_id : String   get() = "user_id"
    val Other_id : String   get() = "other_id"
    val Post_id : String   get() = "post_id"

    val Title : String   get() = "title"

    val Phone_No : String   get() = "phone_no"
    val Signup_type : String   get() = "signup_type"
    val App_Type : String   get() = "app_type"


    val Share_Intent_val : Int   get() = 1000
    val Comming_Soon_text : String   get() = "Coming soon...."
    val No_Internet_Connection : String   get() = "Check your internet connection"


}
