package com.grabbt.Utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class UserSharedPrefrences {


    lateinit var context: Context

//    var preferences: UserSharedPrefrences? = null
   lateinit  var userPreferences: SharedPreferences
   lateinit var playeruserPreferences:SharedPreferences
  lateinit   var edit: SharedPreferences.Editor
    lateinit   var playeredit:SharedPreferences.Editor

    companion object {
        var preferences: UserSharedPrefrences? = null
        fun getInstance(context: Context): UserSharedPrefrences {


            if (preferences == null) {
                preferences = UserSharedPrefrences(context.applicationContext)
            }
            return preferences as UserSharedPrefrences
        }


        var preferences1: UserSharedPrefrences? = null
        fun getInstancee(context: Context): UserSharedPrefrences {


            if (preferences1 == null) {
                preferences1 = UserSharedPrefrences(context.applicationContext,"")
            }
            return preferences1 as UserSharedPrefrences
        }
    }


    constructor(context: Context){
        userPreferences =
            context.getSharedPreferences("UserSharedPreferences", Context.MODE_PRIVATE)
        edit = userPreferences.edit()
    }


    constructor(context: Context,s:String){
        playeruserPreferences =
            context.getSharedPreferences("UserSharedPreferencesplayer", Context.MODE_PRIVATE)
        playeredit = playeruserPreferences.edit()
    }

    fun Clear() {
        edit.clear()
        edit.commit()
    }


    fun setname(name: String) {

        edit.putString("name", name)
        edit.commit()
    }

    fun getname(): String {

        return userPreferences.getString("name", "")!!
    }

  fun setlname(name: String) {

        edit.putString("lname", name)
        edit.commit()
    }

    fun getlname(): String {

        return userPreferences.getString("lname", "")!!
    }


    fun setemail(email: String) {
        Log.e("email", email)
        edit.putString("email", email)
        edit.commit()
    }

    fun getemail(): String {
        return userPreferences.getString("email", "")!!
    }

    fun setlogin(login: Boolean?) {
        edit.putBoolean("login", login!!)
        edit.commit()
    }

    fun getlogin(): Boolean? {
        return userPreferences.getBoolean("login", false)
    }


    fun setuserid(id: String) {
        edit.putString("id", id)
        edit.commit()
    }

    fun getuserid(): String {
        return userPreferences.getString("id", "")!!
    }

    fun setnotify_status(notify_status: String) {
        edit.putString("notify_status", notify_status)
        edit.commit()
    }

    fun getnotify_status(): String {
        return userPreferences.getString("notify_status", "1")!!
    }

    fun setgender(gender: String) {
        edit.putString("gender", gender)
        edit.commit()
    }

    fun getgender(): String {
        return userPreferences.getString("gender", "")!!
    }

    fun setmobile(mobile: String) {
        edit.putString("mobile", mobile)
        edit.commit()
    }

    fun getmobile(): String {
        return userPreferences.getString("mobile", "")!!
    }


    fun setimage(image: String) {
        edit.putString("image", image)
        edit.commit()
    }

    fun getimage(): String {
        return userPreferences.getString("image", "")!!
    }


    fun setplayerid(playerid: String) {
        playeredit.putString("playerid", playerid)
        playeredit.commit()
    }

    fun getplayerid(): String {
        return playeruserPreferences.getString("playerid", "")!!
    }

    fun setlanguage(language: String) {
        playeredit.putString("language_dialog", language)
        playeredit.commit()
    }

    fun getlanguage(): String {
        return playeruserPreferences.getString("language_dialog", "en")!!
    }

    fun setdeviceid(deviceid: String) {
        playeredit.putString("deviceid", deviceid)
        playeredit.commit()
    }

    fun getdeviceid(): String {
        return playeruserPreferences.getString("deviceid", "")!!
    }

    fun setcurrency(currency: String) {
        playeredit.putString("currency", currency)
        playeredit.commit()
    }

    fun getcurrency(): String {
        return playeruserPreferences.getString("currency", "OMR")!!
    }


    fun setpromo(promo: String) {
        edit.putString("promo", promo)
        edit.commit()
    }

    fun getpromo(): String {
        return userPreferences.getString("promo", "")!!
    }

}