package com.grabbt.Utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.AudioManager
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.text.InputType
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.android.volley.error.*
import com.google.android.material.snackbar.Snackbar
import com.grabbt.R
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Common {

    companion object {
        internal var PERMISSI = arrayOf(
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA
        )

        // onactivity result
        //  requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=-1  deny
        //requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=0 allow
        fun askpermission(activity: AppCompatActivity, PERMISSIONS: Array<String>) {
            val PERMISSION_ALL = 1
            if (!hasPermissions(activity, *PERMISSIONS)) {
                ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL)
            }
        }

        fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null && permissions != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }


        public fun isEmailValid(email: String): Boolean {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
            }


        fun isValidMobile(phone: String): Boolean {
            return android.util.Patterns.PHONE.matcher(phone).matches()
        }


        fun isJSONValid(test: String): Boolean {
            try {
                JSONObject(test)
            } catch (ex: JSONException) {
            }

            return true
        }

        @Throws(JSONException::class)
        fun ParseString(obj: JSONObject, Param: String): String {
            if (obj.has(Param)) {
                val lastSeen = obj.getString(Param)
                return if (lastSeen != null && !TextUtils.isEmpty(lastSeen) && !lastSeen.equals(
                        "null",
                        ignoreCase = true
                    ) && lastSeen.length > 0
                )
                    obj.getString(Param)
                else
                    ""

            } else
                return ""
        }


        fun Parsintentvalue(key: String, inte: AppCompatActivity): String? {
            val data: String?

            if (inte.intent.hasExtra(key)) {
                data = inte.intent.getStringExtra(key)
            } else {
                data = ""
            }

            return data

        }


        fun Snakbar(msg: String, v: View) {
            Snackbar.make(v, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        fun checklength(text: EditText): Int {

            return text.text.toString().trim { it <= ' ' }.length
        }


        fun getTimeAgo(time: Long, ctx: Context): String? {

            //        if(date == null) {
            //            return null;
            //        }
            //
            //        long time = date.getTime();
            //        Log.e("time",time+"");
            val curDate = currentDate()
            val now = curDate.time
            if (time > now || time <= 0) {
                return null
            }

            val dim = getTimeDistanceInMinutes(time)

            var timeAgo: String? = null

            if (dim == 0) {
                timeAgo = "Just Now"
            } else if (dim == 1) {
                return "1 " + ctx.resources.getString(R.string.date_util_unit_minute)
            } else if (dim >= 2 && dim <= 44) {
                timeAgo = dim.toString() + " " + ctx.resources.getString(R.string.date_util_unit_minutes)
            } else if (dim >= 45 && dim <= 89) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_about) + " " + ctx.resources.getString(R.string.date_util_term_an) + " " + ctx.resources.getString(
                        R.string.date_util_unit_hour
                    )
            } else if (dim >= 90 && dim <= 1439) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_about) + " " + Math.round((dim / 60).toFloat()) + " " + ctx.resources.getString(
                        R.string.date_util_unit_hours
                    )
            } else if (dim >= 1440 && dim <= 2519) {
                timeAgo = "1 " + ctx.resources.getString(R.string.date_util_unit_day)
            } else if (dim >= 2520 && dim <= 43199) {
                timeAgo =
                    Math.round((dim / 1440).toFloat()).toString() + " " + ctx.resources.getString(R.string.date_util_unit_days)
            } else if (dim >= 43200 && dim <= 86399) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_about) + " " + ctx.resources.getString(R.string.date_util_term_a) + " " + ctx.resources.getString(
                        R.string.date_util_unit_month
                    )
            } else if (dim >= 86400 && dim <= 525599) {
                timeAgo =
                    Math.round((dim / 43200).toFloat()).toString() + " " + ctx.resources.getString(R.string.date_util_unit_months)
            } else if (dim >= 525600 && dim <= 655199) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_about) + " " + ctx.resources.getString(R.string.date_util_term_a) + " " + ctx.resources.getString(
                        R.string.date_util_unit_year
                    )
            } else if (dim >= 655200 && dim <= 914399) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_over) + " " + ctx.resources.getString(R.string.date_util_term_a) + " " + ctx.resources.getString(
                        R.string.date_util_unit_year
                    )
            } else if (dim >= 914400 && dim <= 1051199) {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_almost) + " 2 " + ctx.resources.getString(R.string.date_util_unit_years)
            } else {
                timeAgo =
                    ctx.resources.getString(R.string.date_util_prefix_about) + " " + Math.round((dim / 525600).toFloat()) + " " + ctx.resources.getString(
                        R.string.date_util_unit_years
                    )
            }
            return if (timeAgo == "Just Now") {
                timeAgo
            } else {
                timeAgo + " " + ctx.resources.getString(R.string.date_util_suffix)
            }
        }

        fun timestamptodate(inputDate: String): Date? {
            Log.e("timestamp", getDate(java.lang.Long.parseLong(inputDate))!!.toString() + "")
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            var date: Date? = null
            try {
                date = simpleDateFormat.parse(inputDate)

            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("dateexception", "", e)
                //throw new IllegalAccessException("Error in parsing date");
            }

            return date
        }

        fun getDate(time: Long): Date? {
            loggg("date3", time.toString() + "")
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = time
            val date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString()
            Log.e("date1", date)
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            var datee: Date? = null
            try {
                datee = simpleDateFormat.parse(date)
                Log.e("date2", datee!!.toString() + "")
            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("dateexception", "", e)
                //throw new IllegalAccessException("Error in parsing date");
            }

            return datee
        }

        private fun getTimeDistanceInMinutes(time: Long): Int {
            val timeDistance = currentDate().time - time
            return Math.round((Math.abs(timeDistance) / 1000 / 60).toFloat())
        }

        fun currentDate(): Date {
            val calendar = Calendar.getInstance()
            return calendar.time
        }

        fun hideSoftKeyboard(activity: AppCompatActivity) {
            val inputMethodManager = activity.getSystemService(
                AppCompatActivity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.window.decorView.rootView.windowToken, 0
            )

        }
     fun hideSoftKeyboardinfragment(activity: Activity ) {
            val inputMethodManager = activity.getSystemService(
                Context.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.window.decorView.rootView.windowToken, 0
            )

        }



        fun time_diff(posttime: Long): String {
            var time = ""
            val numberOfDays: Int
            val numberOfHours: Int
            val numberOfMinutes: Int

            try {

                var difference = System.currentTimeMillis() - posttime
                difference = difference / 1000

                numberOfDays = (difference / 86400).toInt()
                numberOfHours = (difference % 86400 / 3600).toInt()
                numberOfMinutes = (difference % 86400 % 3600 / 60).toInt()
                val date = Date(posttime)
                val sdf = SimpleDateFormat("MMM dd", Locale.US)
                val sdf1 = SimpleDateFormat("HH:mm", Locale.US)
                sdf.timeZone = TimeZone.getDefault()
                sdf1.timeZone = TimeZone.getDefault()
                val java_date = sdf.format(date) + " at " + sdf1.format(date)
                Log.e("time_diff", "$numberOfDays,$numberOfHours,$numberOfMinutes")

                if (numberOfDays > 0) {

                    if (numberOfDays > 0) {

                        time = sdf.format(date) + " at " + sdf1.format(date)
                    }
                } else if (numberOfHours > 0) {
                    // if (numberOfHours == 1) {
                    time = "$numberOfHours hr"
                    //                } else {
                    //                    time = numberOfHours + " hours ago";
                    //                }

                } else if (numberOfMinutes > 0) {


                    //                if (numberOfMinutes == 1) {

                    time = "$numberOfMinutes min"
                    //                } else {
                    //                    time = numberOfMinutes + " minutes ago";
                    //                }
                } else {
                    time = "Just now"
                }

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("excptn", "excptn", e)
            }

            return time
        }

        fun isCallActive(context: Context): Boolean {
            val manager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

            return manager.mode == AudioManager.MODE_IN_CALL
        }


//        fun isNetworkConnected(context: Context): Boolean {
//            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//
//            return cm.activeNetworkInfo != null
//        }


        fun set_Image_bytearray(ImageUri: Uri, context: Context): ByteArray {
            var b = ByteArray(0)
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, ImageUri)

                val baos = ByteArrayOutputStream()
                val nh = (bitmap.height * (512.0 / bitmap.width)).toInt()
                val scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true)
                scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                b = baos.toByteArray()
                Log.e("byteArray", b.toString() + "")

            } catch (e: Exception) {
                Log.e("e", "e", e)
                e.printStackTrace()
            }

            return b
        }

        fun Image_Loading_Url(url: String?): String {
            var URL = "abc"
            if (url != null) {


                if (url.length > 0) {
                    if (url.contains("https://")) {
                        URL = url
                    } else if (url.contains("emulated/0")) {
                        URL = url
                    } else if (url.contains("file:///data/user/0/")) {
                        URL = url
                    } else if (url.contains("/storage/")) {
                        URL = url
                    } else {
                                        URL = "https://grabbtapp.com/images/" + url;
                    }
                }
            }
            return URL
        }

        fun volleyerror(volleyError: VolleyError): String {
            var message: String? = "Somthing went wrong"
            if (volleyError is NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is ServerError) {
                message = "The server could not be found. Please try again after some time!!"
            } else if (volleyError is AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is ParseError) {
                message = "Parsing error! Please try again after some time!!"
            } else if (volleyError is NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection."
            }
            return message!!
        }

        fun loggg(tag: String, msg: String) {
            Log.e(tag, msg)

        }


        fun enableInputVisiblePassword(editText: EditText, imageView: ImageView) {
            val INPUT_TYPE_VISIBLE_PASSWORD =
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
            val INPUT_TYPE_HIDDEN_PASSWORD = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            if (editText.inputType == INPUT_TYPE_VISIBLE_PASSWORD) {
                val cache = editText.typeface
                editText.inputType = INPUT_TYPE_HIDDEN_PASSWORD
                editText.typeface = cache
                //            imageView.setImageResource(R.drawable.eye_vision_off);
            } else {
                val cache = editText.typeface
                editText.inputType = INPUT_TYPE_VISIBLE_PASSWORD
                editText.typeface = cache
                //            imageView.setImageResource(R.drawable.eye_vision_on);}
            }

        }


        fun Convertprice( price : Float,context: Context): String{
            var resultval : String = ""
            var formate : String ="%.2f"
            var df =  DecimalFormat("##.00")
            val prefrences=UserSharedPrefrences.getInstancee(context)

            if (prefrences.getcurrency().equals("OMR")){
//            pmr
                resultval = "OMR "+ df.format(price).toString()
            }else if (prefrences.getcurrency().equals("BHD")){
//            bhd
                resultval = "BHD "+df.format( (price * 0.98))
            }else if (prefrences.getcurrency().equals("QAR")){
//            QAR
                resultval = "QAR "+df.format( (price * 9.46))
            }else if (prefrences.getcurrency().equals("SAR")){
//            QAR
                resultval = "SAR "+df.format( (price * 9.74).toString())
            }else if (prefrences.getcurrency().equals("AED")){
//            AED
                resultval = "AED "+df.format((price * 9.54)).toString()
            }else if (prefrences.getcurrency().equals("KWD")){
//            KWD
                resultval = "KWD "+df.format( (price * 0.79)).toString()
            }

            return resultval
        }


        fun calculate_gpoints( price : Float): String{
            var gpoints : String = ""

            gpoints = (price/100).toString()
            return gpoints
        }
    }


}