package com.grabbt

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import kotlinx.android.synthetic.main.contact_us_activity.*
import org.json.JSONObject

class ContactActivity : AppCompatActivity() ,View.OnClickListener, ParamKeys {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contact_us_activity)


        ivback.setOnClickListener(this)
        bt_submit.setOnClickListener(this)
    }
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.ivback ->{
                finish()
            }
            R.id.bt_submit ->{
                check()
            }
        }
    }


    fun check(){
        if (et_name.text.trim().length>0 && et_email.text.trim().length>0 &&
            et_mobile.text.trim().length>0 && et_message.text.trim().length>0){

            if(Common.isEmailValid(et_email.text.trim().toString()))
            {
                if( et_mobile.text.trim().length>9)
                {
                    submitData()
                }
                else
                {
                    Common.Snakbar("Please enter valid phone number",et_name)
                }
            }
            else{
                Common.Snakbar("Please enter valid email address",et_name)
            }


        }else{
            Common.Snakbar("Please fill all fields",et_name)
        }

    }

    private fun submitData() {
        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"my_feedback", Response.Listener<String>
            {
                    response ->

                Common.loggg("res",response);
                try {


                    val obj= JSONObject(response)
                    if (obj.getString("status").equals("true"))
                    {
                        Common.Snakbar(obj.getString("message") ,ll_view);
                        finish()
                    }else if (obj.getString("status").equals("false")){
                        Common.Snakbar(obj.getString("message") ,ll_view);
                    }

                }catch (e : Exception){
                    loggg("exce",e.toString())
                }

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),ll_view);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["name"] = et_name.text.toString()
                param["email"] = et_email.text.toString().trim()
                param["phone"] = et_mobile.text.toString().trim()
                param["message"]=et_message.text.toString()

                Common.loggg("params",param.toString())
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }


}