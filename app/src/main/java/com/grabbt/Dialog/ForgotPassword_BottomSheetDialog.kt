package com.grabbt.Dialog

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.CustomClass.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.grabbt.OTP_Verification_Activity
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.forgotpass_activity.view.*
import kotlinx.android.synthetic.main.forgotpass_activity.view.et_email
import org.json.JSONObject
import java.util.HashMap

class ForgotPassword_BottomSheetDialog: RoundedBottomSheetDialogFragment() ,View.OnClickListener ,ParamKeys{



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.forgotpass_activity, container, false)

        view.tv_submit.setOnClickListener(this)
        return view
    }



    override fun onClick(p0: View?) {
        checkvalidation()
    }

    private fun checkvalidation(){
        Common.hideSoftKeyboardinfragment(activity!!)
         if ( Common.isEmailValid(view!!.et_email.text.toString().trim())){
            Send_request()
        }
        else {
            Toast.makeText(activity!!, "Please enter valid email",Toast.LENGTH_LONG).show()

        }
    }


    fun Send_request(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"forgot_password", Response.Listener<String>
            {
                    response ->

                Common.loggg("forgot_password",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")) {

                    val intent = Intent(activity!!,OTP_Verification_Activity::class.java)
                    intent.putExtra("otp",obj.getJSONObject("data").getString("otp"))
                    intent.putExtra("id",obj.getJSONObject("data").getString("id"))
                    intent.putExtra("email",obj.getJSONObject("data").getString("email"))
                    startActivity(intent)
                    Toast.makeText(activity!!,"OTP has been sent on your registered email",Toast.LENGTH_LONG).show()
                    dismiss()
                }else if (obj.getString("status").equals("false")){

                    Toast.makeText(activity!!,obj.getString("message"),Toast.LENGTH_LONG).show()
                }
                pd.cancel()

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);

                Toast.makeText(activity!!, Common.volleyerror(error),Toast.LENGTH_LONG).show()

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["email"] = view!!.et_email.text.toString().trim()
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

}