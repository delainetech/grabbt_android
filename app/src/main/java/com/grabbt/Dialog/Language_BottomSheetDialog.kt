package com.grabbt.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grabbt.CustomClass.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.grabbt.R

class Language_BottomSheetDialog: RoundedBottomSheetDialogFragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.language_dialog, container, false)
    }
}