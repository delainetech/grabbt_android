package com.grabbt.Dialog

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grabbt.CustomClass.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.grabbt.R
import com.grabbt.Register_Activity
import kotlinx.android.synthetic.main.dialog_gender.view.*
import kotlinx.android.synthetic.main.signup_activity.*

class Gender_BottomSheetDialog: RoundedBottomSheetDialogFragment() ,View.OnClickListener{



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.dialog_gender, container, false)
        initl(view)
        return  view;
    }

    fun initl(view: View){
        view!!.rlmale.setOnClickListener(this)
        view!!.rlfmale.setOnClickListener(this)
        view!!.rlother.setOnClickListener(this)
        view!!.tvcontinue.setOnClickListener(this)

    }


    override fun onClick(p0: View?) {
       when(p0!!.id){
           R.id.rlmale ->{
               changeicon(0)
               activity!!.et_gender.setText("Male")
           }
           R.id.rlfmale ->{
               changeicon(1)
               activity!!.et_gender.setText("Female")
           }
           R.id.rlother ->{
               changeicon(2)
               activity!!.et_gender.setText("Other")
           }
           R.id.tvcontinue ->{
             dismiss()
           }

       }
    }


    fun changeicon(pos : Int){

        if (pos==0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivfmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivother.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))

            }
        }else if (pos==1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivfmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivother.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))

            }

        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivfmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivother.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))

            }
        }

    }
}