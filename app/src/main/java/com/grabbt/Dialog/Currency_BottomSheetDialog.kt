package com.grabbt.Dialog

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grabbt.CustomClass.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.grabbt.R
import com.grabbt.Register_Activity
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.dialog_currency.view.*
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.profile_fragment.view.*

class Currency_BottomSheetDialog: RoundedBottomSheetDialogFragment() ,View.OnClickListener{


  lateinit var  prefrences :UserSharedPrefrences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.dialog_currency, container, false)

        initl(view)
        return  view;
    }

    fun initl(view: View){
         prefrences =UserSharedPrefrences.getInstancee(activity!!)
        view!!.rlbd.setOnClickListener(this)
        view!!.rlomr.setOnClickListener(this)
        view!!.rlqae.setOnClickListener(this)
        view!!.rlsar.setOnClickListener(this)
        view!!.rlaed.setOnClickListener(this)
        view!!.rlkwd.setOnClickListener(this)
        view!!.tvcontinue.setOnClickListener(this)


        if (prefrences.getcurrency().equals("OMR")){
//            pmr
            changeicon(1,view)
        }else if (prefrences.getcurrency().equals("BHD")){
//            bhd
            changeicon(0,view)
        }else if (prefrences.getcurrency().equals("QAR")){
//            QAR
            changeicon(2,view)
        }else if (prefrences.getcurrency().equals("SAR")){
//            QAR
            changeicon(3,view)
        }else if (prefrences.getcurrency().equals("AED")){
//            AED
            changeicon(4,view)
        }else if (prefrences.getcurrency().equals("KWD")){
//            KWD
            changeicon(5,view)
        }
    }


    override fun onClick(p0: View?) {

       when(p0!!.id){
           R.id.rlbd ->{
               changeicon(0,view!!)

           }
           R.id.rlomr ->{
               changeicon(1,view!!)
           }
           R.id.rlqae ->{
               changeicon(2,view!!)
           }
          R.id.rlsar ->{
               changeicon(3,view!!)
           }
          R.id.rlaed ->{
               changeicon(4,view!!)
           }
          R.id.rlkwd ->{
               changeicon(5,view!!)
           }
           R.id.tvcontinue ->{
             dismiss()
           }

       }
    }


    fun changeicon(pos : Int ,view: View){

        if (pos==0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))


                prefrences.setcurrency("BHD")

            }
        }else if (pos==1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                prefrences.setcurrency("OMR")
            }

        }else if (pos==2){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                prefrences.setcurrency("QAR")
            }

        }else if (pos==3){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                prefrences.setcurrency("SAR")
            }

        }else if (pos==4){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                prefrences.setcurrency("AED")
            }

        }
        else if (pos==4){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view!!.ivbd.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivomr.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivqar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivsar.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivaed.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view!!.ivkwd.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                prefrences.setcurrency("KWD")
            }

        }
        activity!!.tv_currency_name.setText(prefrences.getcurrency())
    }
}