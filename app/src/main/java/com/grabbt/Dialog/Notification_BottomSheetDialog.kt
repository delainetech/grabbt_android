package com.grabbt.Dialog

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.CustomClass.RounderBorderDialog.RoundedBottomSheetDialogFragment
import com.grabbt.R
import com.grabbt.Register_Activity
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.dialog_notification.view.*
import org.json.JSONObject
import java.util.HashMap

class Notification_BottomSheetDialog: RoundedBottomSheetDialogFragment() ,View.OnClickListener ,ParamKeys{

    lateinit var prefrences: UserSharedPrefrences
    lateinit var view1: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.dialog_notification, container, false)
        view1=view
        initl(view)
        return  view;
    }

    fun initl(view: View){
        prefrences =UserSharedPrefrences.getInstance(activity!!)
        view.rlmale.setOnClickListener(this)
        view.rlfmale.setOnClickListener(this)

        view.tvcontinue.setOnClickListener(this)


        changeicon(prefrences.getnotify_status().toInt())
    }


    override fun onClick(p0: View?) {
       when(p0!!.id){
           R.id.rlmale ->{
               changeicon(0)
               notification()
           }
           R.id.rlfmale ->{
               changeicon(1)
               notification()
           }

           R.id.tvcontinue ->{
             dismiss()
           }

       }
    }


    fun changeicon(pos : Int){

        if (pos==0){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view1.ivmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))
                view1.ivfmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))

            }
        }else if (pos==1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view1.ivmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_unfilled))
                view1.ivfmale.setImageDrawable(activity!!.getDrawable(R.drawable.check_filled))

            }

        }

    }

    fun notification(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object : StringRequest(
         Method.POST,baseurl+"notify_status", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("g_points",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                prefrences.setnotify_status(obj.getJSONObject("data").getString("notify_status"))

                }
                else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!.ivmale);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!.ivmale);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

}