package com.grabbt

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Dialog.Gender_BottomSheetDialog
import com.grabbt.Dialog.Language_BottomSheetDialog
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import org.json.JSONObject
import java.util.HashMap
import com.heetch.countrypicker.Country
import com.heetch.countrypicker.CountryPickerCallbacks
import com.heetch.countrypicker.CountryPickerDialog
import com.grabbt.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.profile_activity.*
import java.lang.Exception


class Profile_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys{

    lateinit var mcoxt: Context;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_activity)
        mcoxt=this;



      initial()

    }


    fun initial(){

        prefrences =UserSharedPrefrences.getInstance(this)

        et_firstname.setText(prefrences.getname())
        et_lastname.setText(prefrences.getlname())
       et_gender.setText(prefrences.getgender())
       et_mobileno.setText(prefrences.getmobile())
     et_email.setText(prefrences.getemail())


        tvupgrade.setOnClickListener(this)
        ivback.setOnClickListener(this)

        et_gender.setOnClickListener(this)

        tv_countrycode.setOnClickListener(this)

    }


    override fun onClick(p0: View?) {
        when (p0!!.id){
           R.id.et_gender -> {
               val myRoundedBottomSheet = Gender_BottomSheetDialog()
               myRoundedBottomSheet.show(supportFragmentManager!!, myRoundedBottomSheet.tag)
           }

           R.id.ivback -> {
               finish()
           }

         R.id.tvupgrade -> {
             check()
           }

        R.id.tv_countrycode -> {
            val countryPicker = CountryPickerDialog(this,
                CountryPickerCallbacks { country, flagResId ->
                    tv_countrycode.setText("+"+country.dialingCode)

                })
            countryPicker.show()
                   }

        }

    }


    fun check(){
        if (et_firstname.text!!.trim().length>0 && et_lastname.text!!.trim().length>0 && et_gender.text!!.trim().length>0
            && et_email.text!!.length>0 ){

            if ( Common.isEmailValid(et_email.text.toString().trim())){
                updateprofille()

            }
            else {
                Common.Snakbar("Please enter valid email",view!!);

            }
        }else{
            Common.Snakbar("Please fill all fileds",view);
        }
    }


    lateinit var prefrences: UserSharedPrefrences

    fun updateprofille(){


        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_profile", Response.Listener<String>
            {
                    response ->

                Common.loggg("res",response);
                try {


                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    prefrences.setname(Common.ParseString(obj.getJSONObject("data"),F_name))
                   prefrences.setlname(Common.ParseString(obj.getJSONObject("data"),L_name))
                   prefrences.setgender(Common.ParseString(obj.getJSONObject("data"),Gender))

                    pd.cancel()
                    finish()
                    Toast.makeText(this,"Profile has been updated",Toast.LENGTH_LONG).show()
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }

                }catch (e : Exception){
                loggg("exce",e.toString())
                }

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[User_id] = prefrences.getuserid()
                param[F_name] = et_firstname.text.toString().trim()
                param[L_name] = et_lastname.text.toString().trim()
                param[Gender]=et_gender.text.toString()

                Common.loggg("params",param.toString())
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }



}
