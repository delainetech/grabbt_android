package com.grabbt

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.bumptech.glide.Glide
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.homeproduct_detail.*
import org.json.JSONObject
import java.util.HashMap

class DetailActivity : AppCompatActivity() ,View.OnClickListener ,ParamKeys{

    lateinit var context: Context
    var iswishlist:Boolean =false
    var position:Int =0
      var addedquantity:Int =0
       var order_count:Int =0
       var quantity:Int =0
        var idstr:String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.homeproduct_detail)

        context=this
        ivback.setOnClickListener(this)
        tvprice_details.setOnClickListener(this)
        tv_productdetail.setOnClickListener(this)
        tvfavriout.setOnClickListener(this)
        ivadd.setOnClickListener(this)
        tv_addtocart.setOnClickListener(this)
        ivremove.setOnClickListener(this)


//        int.putExtra("product_spec",list.get(position).product_spec)
//        int.putExtra("prize_spec",list.get(position).prize_spec)

        position=intent.getIntExtra("pos",0)
        addedquantity=intent.getIntExtra("addedquantity",0)
        order_count=intent.getIntExtra("order_count",0)
         quantity=intent.getIntExtra("quantity",0)
       iswishlist= intent.getBooleanExtra("isWishlist",false)
        idstr=intent.getStringExtra("id")
        initi()
    }

    fun initi(){

        setviewdetails(0)


        if (addedquantity>0){
            tv_addtocart.visibility = View.GONE
            lladdicon.visibility = View.VISIBLE
            tvquantity.setText(addedquantity.toString())

        }else{
            lladdicon.visibility = View.GONE
            tv_addtocart.visibility = View.VISIBLE

        }
    }
    override fun onClick(p0: View?) {
        when (p0!!.id){
            R.id.ivback ->{
             finish()
            }

             R.id.ivadd ->{

                 if (addedquantity!= (quantity-order_count))
                 {

                addedquantity =addedquantity+1
                     tvquantity.setText(addedquantity.toString())
                 }

                 }

                 R.id.tv_addtocart ->{

                 if (addedquantity!= (quantity-order_count))
                 {

                addedquantity =addedquantity+1
                     tvquantity.setText(addedquantity.toString())
                     tv_addtocart.visibility = View.GONE
                     lladdicon.visibility = View.VISIBLE
                     tvquantity.setText(addedquantity.toString())
                 }

                 }


                  R.id.ivremove ->{

                      addedquantity =addedquantity-1
                      if (addedquantity==0) {
                          lladdicon.visibility = View.GONE
                          tv_addtocart.visibility = View.VISIBLE
                          tvquantity.setText(addedquantity.toString())

                      }
                  }

            R.id.tvfavriout ->{

            if (iswishlist){
                iswishlist=false
                tvfavriout.setImageDrawable(resources.getDrawable(R.drawable.ic_like_unselected))
            }else{
                iswishlist=true
                tvfavriout.setImageDrawable(resources.getDrawable(R.drawable.ic_like_whishlist))
            }
            addtowishlist(tvfavriout,idstr)
            }
           R.id.tvprice_details ->{
               setviewdetails(0)
            }
            R.id.tv_productdetail ->{
                setviewdetails(1)
            }

        }
    }


    fun setviewdetails(position:Int){

        if (position==0){
            tvprice_details.background = resources.getDrawable(R.drawable.details_pricedetail_red_left)
            tv_productdetail.background =resources. getDrawable(R.drawable.details_pricedetail_lightred_right)
            tvprice_details.setTextColor(resources.getColor(R.color.white))
            tv_productdetail.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            Glide.with(context).load(Common.Image_Loading_Url(intent.getStringExtra("prize_image")))
                .into(ivimage)
            tvprizetital.setText(intent.getStringExtra("prize_title"))
            tvdes.setText(intent.getStringExtra("prize_disc"))
            tvgetachance.visibility = View.VISIBLE

        }else{
            tvgetachance.visibility = View.INVISIBLE
            tvprice_details.background =resources. getDrawable(R.drawable.details_pricedetail_lightred_left)
            tv_productdetail.background =resources. getDrawable(R.drawable.details_pricedetail_red_right)
            tvprice_details.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            tv_productdetail.setTextColor(resources.getColor(R.color.white))

            Glide.with(context).load(Common.Image_Loading_Url(intent.getStringExtra("product_image")))
                .into(ivimage)
            tvprizetital.setText(intent.getStringExtra("product_title"))
            tvdes.setText(intent.getStringExtra("product_disc"))
        }

        if (iswishlist){
            tvfavriout.setImageDrawable(resources.getDrawable(R.drawable.ic_like_whishlist))
        }else{
            tvfavriout.setImageDrawable(resources.getDrawable(R.drawable.ic_like_unselected))
        }

        tvprice.setText(Common.Convertprice(intent.getStringExtra("price").toFloat(),context).toString())
        loggg("quantity",quantity.toString()+" , "+order_count.toString())
        tvleft.setText("Only "+(quantity - order_count)+" left")


    }



    fun addtowishlist(view : View,id : String){


        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_wishlist", Response.Listener<String>
            {
                    response ->

                loggg("update_wishlist",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }


    override fun onBackPressed() {
        super.onBackPressed()

        var int=Intent()
        int.putExtra("iswishlist",iswishlist)
        int.putExtra("position",position)
        int.putExtra("addedquantity",addedquantity.toString())
        setResult( Activity.RESULT_OK,int)

    }


}