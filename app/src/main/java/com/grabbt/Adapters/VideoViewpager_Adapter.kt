package com.grabbt.Adapters

import android.content.Context
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.grabbt.Models.HomeProducts
import com.grabbt.R


class VideoViewpager_Adapter(val context: Context?, val list: ArrayList<HomeProducts>) : RecyclerView.Adapter<VideoViewpager_Adapter.ViewHolder>() {
    private val BANDWIDTH_METER = DefaultBandwidthMeter()
    private var playbackPosition: Long = 0
    private var currentWindow: Int = 0
    private var playWhenReady = true





    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val item = View.inflate(parent.context, R.layout.videoview, null)
        val lp = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        item.layoutParams = lp

        return ViewHolder(item)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

//        holder.customProgres.setMaximumPercentage(0.40f)

     holder.   playerView!!.setPlayer(holder.player)
        holder.    player!!.setPlayWhenReady(playWhenReady)
        holder.   player!!.seekTo(currentWindow, playbackPosition)
      holder .playerView!!.useController = false
        val mediaSource =
              buildMediaSource(Uri.parse("https://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4"))
        holder.player!!.prepare(mediaSource, true, false)
    }

    override fun getItemCount(): Int {
        return 2
    }

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {
        var player: SimpleExoPlayer? = ExoPlayerFactory.newSimpleInstance(
            DefaultRenderersFactory(itemView.context),
            DefaultTrackSelector(),
            DefaultLoadControl()
        );
        var playerView: SimpleExoPlayerView? =  itemView.findViewById(R.id.video_view)



    }

    private fun buildMediaSource(uri: Uri): MediaSource {

        val userAgent = "exoplayer-codelab"

        if (uri.lastPathSegment!!.contains("mp3") || uri.lastPathSegment!!.contains("mp4")) {
            return ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else if (uri.lastPathSegment!!.contains("m3u8")) {
            return HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else {
            val dashChunkSourceFactory = DefaultDashChunkSource.Factory(
                DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)
            )
            val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
            return DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory)
                .createMediaSource(uri)
        }
    }

}