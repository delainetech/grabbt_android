package com.grabbt.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.Tickets
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.tickets_item.view.*


class Tickettems_Adapter(val context: Context?, val list: ArrayList<Tickets>) : RecyclerView.Adapter<Tickettems_Adapter.ViewHolder>() {


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {


        val tv_prizetital=itemView.tv_prizetital
        val ivprice=itemView.ivprice
        val tvtcnumber=itemView.tvtcnumber
        val tvproductname=itemView.tvproductname
        val tvleft=itemView.tvleft
        val tvdate=itemView.tvdate
        val progress=itemView.progress
        lateinit var  prefrences : UserSharedPrefrences

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tickets_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvleft.setText((list.get(position).quantity.toInt()-list.get(position).order_count.toInt()).toString()+" Left out of "+list.get(position).quantity)
        holder.prefrences = UserSharedPrefrences.getInstance(context!!)

        holder.progress.max =list.get(position).quantity.toFloat()
        holder.progress.progress =list.get(position).order_count.toFloat()


        Glide.with(context!!).load(Common.Image_Loading_Url(list.get(position).prize_image)).into(holder.ivprice);

        holder.tv_prizetital.setText(list.get(position).prize_title)
       holder.tvtcnumber.setText(list.get(position).tickets)
       holder.tvproductname.setText(list.get(position).product_title)

       holder. tvdate.setText(Common.time_diff(list.get(position).timestamp.toLong()))

    }

    override fun getItemCount(): Int {
        loggg("size",list.size.toString())
        return list.size
    }

}