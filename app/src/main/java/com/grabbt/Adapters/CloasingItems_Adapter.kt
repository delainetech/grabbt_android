package com.grabbt.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.campaign_closingsoon_item.view.*


class CloasingItems_Adapter(val context: Context?, val list: ArrayList<HomeProducts>) : RecyclerView.Adapter<CloasingItems_Adapter.ViewHolder>() {


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {


        val tv_prizetital=itemView.tv_prizetital

        val tv_leftqun=itemView.tv_leftqun
        val tvleft=itemView.tvleft
        val progress=itemView.progress
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.campaign_closingsoon_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_leftqun.setText("Only "+(list.get(position).quantity.toInt()-list.get(position).order_count.toInt()).toString()+" left")
        holder.tvleft.setText((list.get(position).quantity.toInt()-list.get(position).order_count.toInt()).toString()+" Left out of "+list.get(position).quantity)

        holder.progress.max =list.get(position).quantity.toFloat()
        holder.progress.progress =list.get(position).order_count.toFloat()
        holder.tv_prizetital.setText(list.get(position).prize_title)

    }

    override fun getItemCount(): Int {
        return list.size
    }

}