package com.grabbt.Adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.bumptech.glide.Glide
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.WishList
import com.grabbt.R
import com.grabbt.Tabs.WishList_Fragment
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.wishlist_item.view.*
import org.json.JSONObject
import java.util.HashMap


class WishItems_Adapter(val context: Context?, val list: ArrayList<WishList>,val text:TextView,val rec:RecyclerView) :
    RecyclerView.Adapter<WishItems_Adapter.ViewHolder>() ,ParamKeys{


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val iv_removewishlist=itemView.iv_removewishlist
        val iv_addtocart=itemView.iv_addtocart
        val tvproducttital=itemView.tvproducttital
        val tv_prizetital=itemView.tv_prizetital
        val tv_price=itemView.tv_price
        val iv_img=itemView.iv_img

                val tvsold=itemView.tvsold
        val progress=itemView.progress
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.wishlist_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.tvsold.setText("Sold "+list.get(position).order_count+" out of "+list.get(position).quantity)
        holder.progress.max =list.get(position).quantity.toFloat()
        holder.progress.progress =list.get(position).order_count.toFloat()

        holder.tvproducttital.setText(list.get(position).product_title)
   holder.tv_prizetital.setText(list.get(position).prize_title)
     holder.tv_price.setText(Common.Convertprice(list.get(position).price.toFloat(),context!!))

        holder.iv_removewishlist.setOnClickListener(View.OnClickListener {view ->

            Wishlist_alert(position,holder.itemView)

        })
        holder.iv_addtocart.setOnClickListener(View.OnClickListener {view ->
            if (list.get(position).addedquantity.toInt()!= (list.get(position).quantity.toInt()-list.get(position).order_count.toInt())) {

                list.get(position).addedquantity =
                    (list.get(position).addedquantity.toInt() + 1).toString()
                addtocart(holder.itemView, list.get(position).id,
                    list.get(position).addedquantity)

            }
        })

        Glide.with(context!!).load(Common.Image_Loading_Url(list.get(position).prize_image)).into(holder.iv_img);

    }

    override fun getItemCount(): Int {
        return list.size
    }


    fun removewishlist(view : View,id : String){




        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_wishlist", Response.Listener<String>
            {
                    response ->

                Common.loggg("update_wishlist",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }

    fun addtocart(view : View,id : String,qu : String){




        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_cart", Response.Listener<String>
            {
                    response ->

                Common.loggg("update_cart",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                Toast.makeText(context,"Item added to wishlist",Toast.LENGTH_LONG).show()
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id
                param["quantity"]=qu
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }


       fun Wishlist_alert(position: Int,view: View) {

    val alertDialog = AlertDialog.Builder(context!!)

    alertDialog.setMessage("You you sure you want to remove this item from your wish list")
    alertDialog.setPositiveButton(context.getString(R.string.yes),
    DialogInterface.OnClickListener { dialog, which ->

        removewishlist(view, list.get(position).id)
        list.removeAt(position)
        notifyItemChanged(position)

        if (list.size==0){
            rec.visibility =View.GONE
            text.visibility =View.VISIBLE
        }

    })


           alertDialog.setNegativeButton(
               "No"
           ) { dialog, which -> dialog.cancel() }
           val alertDialog1 = alertDialog.create()
    alertDialog1.show()

//    alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent))
//    alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent))
}

}
