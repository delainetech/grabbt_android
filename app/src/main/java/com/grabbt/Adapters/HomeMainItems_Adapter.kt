package com.grabbt.Adapters

import android.content.Context
import android.content.Intent
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.grabbt.DetailActivity
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import com.grabbt.Tabs.Home_Fragment
import kotlinx.android.synthetic.main.homescreenitems_listings.view.*
import com.like.LikeButton
import com.like.OnLikeListener
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.AlphaAnimation
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.bumptech.glide.Glide
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.profile_fragment.view.*
import org.json.JSONObject
import java.util.HashMap


class HomeMainItems_Adapter(val context: Context?, val list: ArrayList<HomeProducts>,val frg: Home_Fragment)
    : RecyclerView.Adapter<HomeMainItems_Adapter.ViewHolder>() ,ParamKeys {




    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_pricedetail=itemView.tv_pricedetail
        val tvprice=itemView.tvprice
        val like=itemView.like
          val ivwishlist=itemView.ivwishlist
        val tv_prizetital=itemView.tv_prizetital
       val tvaddtocart=itemView.tvaddtocart
      val tvquantity=itemView.tvquantity
      val lladdtocart=itemView.lladdtocart
      val lladdicon=itemView.lladdicon
       val ivadd=itemView.ivadd
        val ivremove=itemView.ivremove
        val circular_progress=itemView.circular_progress
         val ivprice=itemView.ivprice
         val tv_leftqun=itemView.tv_leftqun
         val tvleft=itemView.tvleft
         val progress=itemView.progress
         val tvsoldquantity=itemView.tvsoldquantity
         val tvtotalquantity=itemView.tvtotalquantity
        lateinit var  prefrences : UserSharedPrefrences

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.homescreenitems_listings, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvsoldquantity.setText(list.get(position).order_count)
        holder.tvtotalquantity.setText(list.get(position).quantity)

        holder.tv_leftqun.setText("Only "+(list.get(position).quantity.toInt()-list.get(position).order_count.toInt()).toString()+" left")
        holder.tvleft.setText((list.get(position).quantity.toInt()-list.get(position).order_count.toInt()).toString()+" Left out of "+list.get(position).quantity)
        holder.prefrences = UserSharedPrefrences.getInstance(context!!)

       holder.circular_progress.maxProgress =list.get(position).quantity.toDouble()
        holder.circular_progress.setCurrentProgress(list.get(position).order_count.toDouble())

        holder.progress.max =list.get(position).quantity.toFloat()
         holder.progress.progress =list.get(position).order_count.toFloat()

        if (list.get(position).isWishlist){
            holder.ivwishlist.setImageDrawable(context.resources.getDrawable(R.drawable.ic_like_whishlist))
        }else{
            holder.ivwishlist.setImageDrawable(context.resources.getDrawable(R.drawable.ic_like_unselected))
        }

            if (list.get(position).addedquantity.toInt()==0){
                holder.lladdtocart.visibility =View.VISIBLE
                holder.lladdicon.visibility =View.GONE
            }else{
                holder.lladdtocart.visibility =View.GONE
                holder.lladdicon.visibility =View.VISIBLE
            }
        holder.tvquantity.setText(list.get(position).addedquantity)

        holder.tv_pricedetail.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                val int= Intent(context,DetailActivity::class.java)

                int.putExtra("id",list.get(position).id)
                int.putExtra("product_title",list.get(position).product_title)
                int.putExtra("product_disc",list.get(position).product_disc)
                int.putExtra("product_spec",list.get(position).product_spec)
                int.putExtra("product_image",list.get(position).product_image)
               int.putExtra("price",list.get(position).price)
               int.putExtra("quantity",list.get(position).quantity.toInt())
               int.putExtra("order_count",list.get(position).order_count)
               int.putExtra("prize_title",list.get(position).prize_title)
               int.putExtra("prize_disc",list.get(position).prize_disc)
               int.putExtra("prize_spec",list.get(position).prize_spec)
               int.putExtra("prize_image",list.get(position).prize_image)
               int.putExtra("isWishlist",list.get(position).isWishlist)
               int.putExtra("order_count",list.get(position).order_count.toInt())
                int.putExtra("addedquantity",list.get(position).addedquantity.toInt())
                int.putExtra("pos",position)
                frg!!.startActivityForResult(int,11);
            }})
        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                val int= Intent(context,DetailActivity::class.java)

                int.putExtra("id",list.get(position).id)
                int.putExtra("product_title",list.get(position).product_title)
                int.putExtra("product_disc",list.get(position).product_disc)
                int.putExtra("product_spec",list.get(position).product_spec)
                int.putExtra("product_image",list.get(position).product_image)
               int.putExtra("price",list.get(position).price)
               int.putExtra("quantity",list.get(position).quantity.toInt())
               int.putExtra("order_count",list.get(position).order_count)
               int.putExtra("prize_title",list.get(position).prize_title)
               int.putExtra("prize_disc",list.get(position).prize_disc)
               int.putExtra("prize_spec",list.get(position).prize_spec)
               int.putExtra("prize_image",list.get(position).prize_image)
               int.putExtra("isWishlist",list.get(position).isWishlist)
               int.putExtra("order_count",list.get(position).order_count.toInt())
                int.putExtra("addedquantity",list.get(position).addedquantity.toInt())
                int.putExtra("pos",position)
                frg!!.startActivityForResult(int,11);
            }})

        holder.tvaddtocart.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (holder.prefrences.getlogin()!!) {
                if (list.get(position).addedquantity.toInt()!= (list.get(position).quantity.toInt()-list.get(position).order_count.toInt())) {
                    list.get(position).addedquantity =
                        (list.get(position).addedquantity.toInt() + 1).toString()
                    holder.lladdtocart.visibility = View.GONE
                    holder.lladdicon.visibility = View.VISIBLE

                    notifyItemChanged(position)


                        addtocart(
                            holder.itemView,
                            list.get(position).id,
                            list.get(position).addedquantity
                        )
                    }
                }else{
                    Toast.makeText(context,"You need to login first",Toast.LENGTH_LONG).show()
                }
            }})

     holder.ivadd.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (holder.prefrences.getlogin()!!) {

                    if (list.get(position).addedquantity.toInt()!= (list.get(position).quantity.toInt()-list.get(position).order_count.toInt())) {

                    list.get(position).addedquantity =
                        (list.get(position).addedquantity.toInt() + 1).toString()


                    notifyItemChanged(position)

                        addtocart(
                            holder.itemView,
                            list.get(position).id,
                            list.get(position).addedquantity
                        )
                    }
                }
                else{
                    Toast.makeText(context,"You need to login first",Toast.LENGTH_LONG).show()
                }
            }})

         holder.ivremove.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (holder.prefrences.getlogin()!!) {

                    list.get(position).addedquantity = (list.get(position).addedquantity.toInt() - 1).toString()

                if (list.get(position).addedquantity.toInt()==0){
                    holder.lladdicon.visibility =View.GONE
                    holder.lladdtocart.visibility =View.VISIBLE

                }

                notifyItemChanged(position)



                   addtocart(holder.itemView, list.get(position).id,list.get(position).addedquantity)
                }else{
                    Toast.makeText(context,"You need to login first",Toast.LENGTH_LONG).show()
                }
            }})


        Glide.with(context).load(Common.Image_Loading_Url(list.get(position).prize_image)).into(holder.ivprice);

        holder.tvprice.setText(Common.Convertprice(list.get(position).price.toFloat(),context))
       if (list.get(position).prize_title.contains("BRAND")) {
           loggg(
               "textsplit",
               list.get(position).prize_title.substring(
                   list.get(position).prize_title.lastIndexOf("BRAND"),
                   list.get(position).prize_title.length
               )
           )
           val first = list.get(position).prize_title.substring(0,
               list.get(position).prize_title.lastIndexOf("BRAND")
           )
           val next = "<font color='#EE0000'>"+list.get(position).prize_title.substring(
               list.get(position).prize_title.lastIndexOf("BRAND"),
               list.get(position).prize_title.length
           ) +"</font>"
           holder.tv_prizetital.setText(Html.fromHtml(first + next));
       }else if (list.get(position).prize_title.contains("OMR 800")){
           loggg(
               "textsplit",
               list.get(position).prize_title.substring(
                   list.get(position).prize_title.lastIndexOf("OMR 800"),
                   list.get(position).prize_title.length
               )
           )
           val first = list.get(position).prize_title.substring(0,
               list.get(position).prize_title.lastIndexOf("OMR 800")
           )
           val next = "<font color='#EE0000'>"+list.get(position).prize_title.substring(
               list.get(position).prize_title.lastIndexOf("OMR 800"),
               list.get(position).prize_title.length
           ) +"</font>"
           holder.tv_prizetital.setText(Html.fromHtml(first + next));
       }else {
           holder.tv_prizetital.setText(list.get(position).prize_title)
       }




        holder.ivwishlist.setOnClickListener( View.OnClickListener { view ->
            if (holder.prefrences.getlogin()!!) {

            if (list.get(position).isWishlist){
                list.get(position).isWishlist =false
                holder.ivwishlist.setImageDrawable(context.resources.getDrawable(R.drawable.ic_like_unselected))

            }else{
                list.get(position).isWishlist =true
                holder.ivwishlist.setImageDrawable(context.resources.getDrawable(R.drawable.ic_like_whishlist))
            }
            notifyItemChanged(position)


                addtowishlist(holder.itemView, list.get(position).id)
            }
            else {
                Toast.makeText(context,"You need to login first",Toast.LENGTH_LONG).show()
            }

        })


    }

    override fun getItemCount(): Int {
        return list.size
    }




    fun addtowishlist(view : View,id : String){


        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_wishlist", Response.Listener<String>
            {
                    response ->

                loggg("update_wishlist",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id

               return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }

    fun addtocart(view : View,id : String,qu : String){




        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_cart", Response.Listener<String>
            {
                    response ->

                Common.loggg("update_cart",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id
             param["quantity"]=qu
                loggg("params",param.toString())
               return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }


}
