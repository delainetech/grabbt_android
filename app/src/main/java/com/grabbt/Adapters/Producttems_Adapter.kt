package com.grabbt.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.grabbt.DetailActivity
import com.grabbt.Models.Product
import com.grabbt.ProductDetailActivity
import com.grabbt.R
import com.grabbt.Utils.Common
import kotlinx.android.synthetic.main.productlist_item.view.*


class Producttems_Adapter(val context: Context?, val list: ArrayList<Product>) : RecyclerView.Adapter<Producttems_Adapter.ViewHolder>() {


    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val iv_img=itemView.iv_img
        val tv_tital=itemView.tv_tital
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.productlist_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        Glide.with(context!!).load(Common.Image_Loading_Url(list.get(position).image)).into( holder.iv_img);

        holder.tv_tital.setText(list.get(position).title)

        holder.itemView.setOnClickListener( View.OnClickListener { view ->
            val int= Intent(context, ProductDetailActivity::class.java)
                int.putExtra("title",list.get(position).title)
                  int.putExtra("image",list.get(position).image)
                int.putExtra("discription",list.get(position).discription)
                context.startActivity(int)
        })
    }

    override fun getItemCount(): Int {
        return list.size
    }

}