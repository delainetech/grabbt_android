package com.grabbt.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import kotlinx.android.synthetic.main.faq_items.view.*


class Faq_Adapter(val context: Context?) : RecyclerView.Adapter<Faq_Adapter.ViewHolder>() {


    internal var question = arrayOf("What Is Grabbt?", "How can I access Grabbt.com?", "How to find us?",
        "What Is Grabbt?", "How can I access Grabbt.com?", "How to find us?")
     internal var ans = arrayOf("Grabbt is a stationery and garment store with a twist. For every purchase made, Grabbt gives its customers a complimentary Prize Draw ticket where they can win a luxury prize."
         , "Grabbt is available on desktop, smart phone, tablet and via our interactive kiosks."
         , "Grabbt HQ is situated in Dubai. Outlet stores will be coming soon to the region, in the meantime, if you have any inquiries please go to our ‘Contact Us’ page.",
         "Grabbt is a stationery and garment store with a twist. For every purchase made, Grabbt gives its customers a complimentary Prize Draw ticket where they can win a luxury prize."
         , "Grabbt is available on desktop, smart phone, tablet and via our interactive kiosks."
         , "Grabbt HQ is situated in Dubai. Outlet stores will be coming soon to the region, in the meantime, if you have any inquiries please go to our ‘Contact Us’ page.")

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tvtital=itemView.tvtital
        val tvdes=itemView.tvdes
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.faq_items, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvtital.setText(question[position])
        holder.tvdes.setText(ans[position])

    }

    override fun getItemCount(): Int {
        return question.size
    }

}