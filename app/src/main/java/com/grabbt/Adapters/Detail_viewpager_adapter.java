package com.grabbt.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.grabbt.Exo_fragment;

import java.util.ArrayList;

public class Detail_viewpager_adapter extends FragmentStatePagerAdapter {



    Context context;
    public Detail_viewpager_adapter(FragmentManager fm, Context context)
    {
        super(fm);

       this. context=context;
    }

    @Override
    public Fragment getItem(int position) {

            return new Exo_fragment();

        }

    @Override
    public int getCount() {
        return 2;
    }

    //this is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }
}
