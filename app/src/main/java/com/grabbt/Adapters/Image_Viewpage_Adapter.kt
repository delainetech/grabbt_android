package com.grabbt.Adapters

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import java.security.MessageDigest

class Image_Viewpage_Adapter(context: Context,arrayList: ArrayList<String>): PagerAdapter(),ParamKeys
{
    var context:Context
    init{
        this.context=context
    }

    var arrayList:ArrayList<String>
    init{
        this.arrayList=arrayList
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout

    }


    lateinit var  layoutInflater: LayoutInflater

    internal var requestOptions = RequestOptions().fitCenter()


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val itemView = layoutInflater.inflate(R.layout.mediadialogadapter, container, false)
        val imageView = itemView.findViewById(R.id.image) as ImageView
        Glide.with(context).load(Common.Image_Loading_Url(arrayList.get(position)))
            .apply(requestOptions).into(imageView)

        container.addView(itemView)


         return itemView


    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    override fun getCount(): Int {
        loggg("listsize",arrayList.size.toString())
       return  arrayList.size
    }
}