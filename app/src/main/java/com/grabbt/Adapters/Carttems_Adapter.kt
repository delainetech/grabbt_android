package com.grabbt.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.bumptech.glide.Glide
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.CartList
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.cart_item.view.*
import kotlinx.android.synthetic.main.cart_item.view.ivadd
import kotlinx.android.synthetic.main.cart_item.view.ivremove
import kotlinx.android.synthetic.main.cart_item.view.tvquantity
import org.json.JSONObject
import java.util.HashMap


class Carttems_Adapter(val context: Context?, val list: ArrayList<CartList>,val tvempty:TextView,
                       val cardview : CardView , val nested : NestedScrollView
                       ,val tvtotalpro:TextView,val tvtotalticket:TextView,val tv_grandtotal:TextView
                       ,val tvgpoints:TextView) :
    RecyclerView.Adapter<Carttems_Adapter.ViewHolder>() ,ParamKeys {

    var totaltickets:Int =0

     var priceflot:Float = 0.0F

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val iv_img=itemView.iv_img
           val tv_tital=itemView.tv_tital
           val tv_price=itemView.tv_price
           val tv_tickets=itemView.tv_tickets
           val ivadd=itemView.ivadd
          val tvquantity=itemView.tvquantity
          val ivremove=itemView.ivremove


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cart_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.tvquantity.setText(list.get(position).addedquantity)

        Glide.with(context!!).load(Common.Image_Loading_Url(list.get(position).prize_image)).into(holder.iv_img);
        holder.tv_tital.setText(list.get(position).product_title)
       holder.tv_price.setText(Common.Convertprice(list.get(position).price.toFloat(),context))

        holder.ivadd.setOnClickListener( View.OnClickListener { view ->

            if (list.get(position).addedquantity.toInt()!= (list.get(position).quantity.toInt()-list.get(position).order_count.toInt())) {

                list.get(position).addedquantity =
                    (list.get(position).addedquantity.toInt() + 1).toString()


                notifyItemChanged(position)


                    addtocart(
                        holder.itemView,
                        list.get(position).id,
                        list.get(position).addedquantity
                    )

            }
        })

        holder.ivremove.setOnClickListener( View.OnClickListener { view ->

            if (totaltickets==1){
                nested.visibility =View.GONE
                cardview.visibility =View.GONE
                tvempty.visibility =View.VISIBLE

                list.get(position).addedquantity = (list.get(position).addedquantity.toInt() - 1).toString()
                addtocart(holder.itemView, list.get(position).id,list.get(position).addedquantity)
                list.removeAt(position)
                notifyItemChanged(position)
            }else{

            if (list.get(position).addedquantity.toInt()==1){
                list.get(position).addedquantity = (list.get(position).addedquantity.toInt() - 1).toString()
                addtocart(holder.itemView, list.get(position).id,list.get(position).addedquantity)
                list.removeAt(position)
                notifyDataSetChanged()
            } else
            {
            list.get(position).addedquantity = (list.get(position).addedquantity.toInt() - 1).toString()


                addtocart(holder.itemView, list.get(position).id,list.get(position).addedquantity)
            notifyItemChanged(position)


        }
        }
        }
        )

        totaltickets=0

        priceflot=0.0F
        for (i in 0..(list.size-1)){
            totaltickets=  totaltickets + list.get(i).addedquantity.toInt()
            priceflot=priceflot+ (list.get(i).addedquantity.toInt() * list.get(i).price.toFloat())
        }
        tvtotalpro.setText(totaltickets.toString())
        tvtotalticket.setText((totaltickets * 2).toString() )
        tv_grandtotal.setText(Common.Convertprice(priceflot,context))
        tvgpoints.setText("You will earn "+Common.calculate_gpoints(priceflot)+" G-Points from this purchase")
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addtocart(view : View,id : String,qu : String){




        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"update_cart", Response.Listener<String>
            {
                    response ->

                Common.loggg("update_cart",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->

                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val userSharedPreferences= UserSharedPrefrences.getInstance(context!!)
                val param = HashMap<String, String>()
                param["user_id"] = userSharedPreferences.getuserid()
                param["product_id"]=id
                param["quantity"]=qu
               loggg("params", param.toString())
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context!!).addToRequestQueue(stringRequest)
    }



}