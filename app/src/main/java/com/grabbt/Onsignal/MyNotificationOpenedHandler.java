package com.grabbt.Onsignal;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by Kishan on 19-Jun-17.
 */

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {


    Context context;

    public MyNotificationOpenedHandler(Context context) {
        this.context = context;

    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;

        String activityToBeOpened;

        Log.e("onsignaldata1","notification: "+data);
        //While sending a Push notification from OneSignal dashboard
        // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
        //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
        //on the notification, AnotherActivity will be opened.
        //Else, if we have not set any additional data MainActivity is opened.
//        try {
        if (data != null) {

        }

    }

    }
