package com.grabbt.Onsignal;

import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.grabbt.Utils.Common;
import com.grabbt.Utils.UserSharedPrefrences;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Kishan on 19-Jun-17.
 */

public class Onesignal_Myapplication extends MultiDexApplication {

    private static Context context;
    String user_id="";

    UserSharedPrefrences prefrences;

    public static Context getContext() {
        return context;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();

        prefrences=UserSharedPrefrences.Companion.getInstancee(context);

        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler(this))
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                    user_id=userId;
                Log.e("playerid",userId);
                prefrences.setplayerid(user_id);

            }
        });
        // Branch logging for debugging
      ;



    }


}
