package com.grabbt

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.grabbt.Tabs.*
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity() {

    lateinit var bottom_navigation_b : BottomNavigationView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottom_navigation_b =this.bottom_navigation

        supportFragmentManager.beginTransaction().replace(R.id.framelay, Home_Fragment()).commit()

        bottom_navigation_b.setOnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.menu_Home -> {

                    supportFragmentManager.beginTransaction().replace(R.id.framelay, Home_Fragment()).commit()

                }
                R.id.menu_Wishlist -> {
                    supportFragmentManager.beginTransaction().replace(R.id.framelay, WishList_Fragment()).commit()

                }
                R.id.menu_Cart -> {

                    supportFragmentManager.beginTransaction().replace(R.id.framelay, Cart_Fragment()).commit()
                }
                R.id.menu_Tickets -> {
                    supportFragmentManager.beginTransaction().replace(R.id.framelay, Ticket_Fragment()).commit()
                }
                R.id.menu_Profile -> {

                    supportFragmentManager.beginTransaction().replace(R.id.framelay, Profile_Fragment()).commit()

                }
            }
            return@setOnNavigationItemSelectedListener true
        }

    }


    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }



}