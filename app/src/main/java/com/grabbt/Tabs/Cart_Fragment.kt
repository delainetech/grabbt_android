package com.grabbt.Tabs

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.Carttems_Adapter
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.EndlessRecyclerViewScrollListener
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.CartList
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.WishList
import com.grabbt.Payment_Activity
import com.grabbt.R
import com.grabbt.SuccessfullsubmitActivity
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.cart_fragment.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class Cart_Fragment : Fragment(),View.OnClickListener , SwipeRefreshLayout.OnRefreshListener ,
    ParamKeys {


    lateinit var recyclerview: RecyclerView
    private lateinit var linearLayoutManager:LinearLayoutManager

    val list: ArrayList<CartList> = ArrayList()
    lateinit var prefrences : UserSharedPrefrences
    var index : Int = 1;
    private lateinit var recyclerViewScrollListener:EndlessRecyclerViewScrollListener
    lateinit var view1: View
    var orderid :String ="";
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.cart_fragment, container, false);
        view1=view
        initil(view)
        return view
    }

    private fun initil(view: View) {
        prefrences = UserSharedPrefrences.getInstance(activity!!)
       orderid= System.currentTimeMillis().toString()
        recyclerview=view.recycler_explore;
        linearLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = linearLayoutManager
        recyclerview.adapter= Carttems_Adapter(context,list, view. tvemptyview, view.cv,view. nv,
                                                    view.tv_totalpro,view.tv_totaltic,view.tv_grandtotal
            ,view.tvgpoints)
        recyclerview.isNestedScrollingEnabled = false
        view.tv_checkout.setOnClickListener(this)




        view.swiprefres.setColorSchemeColors(
            activity!!.resources.getColor(R.color.colorPrimary),
            activity!!.resources.getColor(R.color.colorPrimary), activity!!.resources.getColor(R.color.colorPrimary)
        )
        view.swiprefres.setOnRefreshListener(this)

        if (prefrences.getlogin()!!) {
            get_cart()
        }else{
            checklist()
        }
        recyclerViewScrollListener =
            object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {


                    if (prefrences.getlogin()!!) {
                        index++
                        get_cart()
                    }
                }
            }

        recyclerview.addOnScrollListener(recyclerViewScrollListener)
    }

    var itemid :Int =0
    var price :Float =0f
    var itemquan :String =""
var jsonArray = JSONArray()
    override fun onClick(p0: View?) {

//        checkout()

        for (i in 0..(list.size-1)){
            var jsonObject= JSONObject()
            jsonObject.put("product_id",list.get(i).id)
            jsonObject.put("quantity",list.get(i).addedquantity)
            jsonArray.put(jsonObject)

            itemid =  itemid + list.get(i).price.toInt()
            price =  price + (list.get(i).price.toInt()*list.get(i).addedquantity.toInt())
            loggg("price",price.toString())
          loggg("price",list.get(i).addedquantity)
//            itemquan =  itemquan + list.get(i).addedquantity+","
        }

//        startActivity(Intent(activity,Payment_Activity::class.java)
//            .putExtra("jsonArray",jsonArray.toString())
//            .putExtra("earned",Common.calculate_gpoints(itemid.toFloat()))
//            .putExtra("price",price.toString()))
        loggg("price",price.toString())
        checkout()
//        checkout_paymentgetway()
//        loggg("itemid=",Common.calculate_gpoints(itemid.toFloat()).toString())
     }


    override fun onRefresh() {
        index = 1
        list.clear()
        if (prefrences.getlogin()!!) {

            get_cart()
        }
        view!!.   swiprefres.isRefreshing = false

    }


    fun get_cart(){


        val pd= DelayedProgressDialog()
        if (index==0) {
            pd.show(fragmentManager!!, "")
        }
        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"get_cart", Response.Listener<String>
            {
                    response ->

                loggg("get_cart",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONObject("data").getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                        val jsobj=jsonArray.getJSONObject(i)

                        list.add( CartList(
                            Common.ParseString(jsobj, "cart_id"),
                            Common.ParseString(jsobj, "id"),
                            Common.ParseString(jsobj, "product_title"),
                            Common.ParseString(jsobj, "product_disc"),
                            Common.ParseString(jsobj, "product_spec"),
                            Common.ParseString(jsobj, "product_image"),
                            Common.ParseString(jsobj, "price"),
                            Common.ParseString(jsobj, "quantity"),
                            Common.ParseString(jsobj, "prize_title"),
                            Common.ParseString(jsobj, "prize_spec"),
                            Common.ParseString(jsobj, "prize_image")
                            ,
                            Common.ParseString(jsobj, "prize_video"), Common.ParseString(jsobj, "cart_count")
                            ,
                            Common.ParseString(jsobj, "prize_disc") ,
                            Common.ParseString(jsobj, "order_count")))
                    }
                        checklist()
                    recyclerview.adapter!!.notifyDataSetChanged()

                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }

                pd.cancel()
            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()
                param["page"] =index.toString()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

    fun checklist(){

        if (list.size>0){
            view1. tvemptyview.visibility = View.GONE
            view1.  cv.visibility = View.VISIBLE
            view1. nv.visibility = View.VISIBLE
        }else{
            view1.cv.visibility = View.GONE
            view1. nv.visibility = View.GONE
            view1. tvemptyview.visibility = View.VISIBLE

        }
    }



    fun checkout(){


        val pd= DelayedProgressDialog()

            pd.show(fragmentManager!!, "")
        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"checkout", Response.Listener<String>
            {
                    response ->

                loggg("checkout",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){



                    startActivity(Intent(activity,Payment_Activity::class.java)
                        .putExtra("order_id",obj.getString("order_id"))
                        .putExtra("orderid",orderid).putExtra("price",price.toString()))
//                    Common.Snakbar("Order has been placed",view!!);
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }
                pd.cancel()
                checklist()
            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()
                param["order_id"] = orderid
                param["order_details"] =jsonArray.toString()
                param["earned"] =Common.calculate_gpoints(itemid.toFloat())
                param["spend"] ="0"
                param["price"] =price.toString()
                loggg("params",param.toString())
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }



}