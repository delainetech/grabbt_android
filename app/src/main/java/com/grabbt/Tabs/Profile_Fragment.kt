package com.grabbt.Tabs

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.error.VolleyError
import com.android.volley.request.SimpleMultiPartRequest
import com.android.volley.request.StringRequest
import com.bumptech.glide.Glide
import com.grabbt.*
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Dialog.Currency_BottomSheetDialog
import com.grabbt.Dialog.ForgotPassword_BottomSheetDialog
import com.grabbt.Dialog.Language_BottomSheetDialog
import com.grabbt.Dialog.Notification_BottomSheetDialog
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.profile_activity.*
import kotlinx.android.synthetic.main.profile_fragment.view.*
import org.json.JSONObject
import java.lang.Exception
import java.util.HashMap



class Profile_Fragment : Fragment() ,View.OnClickListener ,ParamKeys {

    lateinit var prefrences: UserSharedPrefrences
     lateinit var prefrences1: UserSharedPrefrences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.profile_fragment, container, false);
        initil(view)
        return view
    }

    private fun initil(view : View) {

        prefrences =UserSharedPrefrences.getInstance(activity!!)
         prefrences1 =UserSharedPrefrences.getInstancee(activity!!)
      view.tv_register .setOnClickListener(this)
      view.tv_language.setOnClickListener(this)
      view.tvchangepass.setOnClickListener(this)
       view.tv_signin.setOnClickListener(this)
        view.ivuser.setOnClickListener(this)
        view.tvlogout.setOnClickListener(this)
     view.tvpersonaldetails.setOnClickListener(this)
     view.tvwishlist.setOnClickListener(this)
      view.tvtickets.setOnClickListener(this)
        view.tv_products.setOnClickListener(this)
      view.tv_notification.setOnClickListener(this)
     view.tv_currency.setOnClickListener(this)
   view.tv_share_apps.setOnClickListener(this)
     view.tvcharity.setOnClickListener(this)
      view.tv_contactus.setOnClickListener(this)
       view.tvgpoints.setOnClickListener(this)
      view.tv_forgotpass.setOnClickListener(this)
   view.tvfaq.setOnClickListener(this)
view.tv_howitwork.setOnClickListener(this)




    }


    override fun onClick(p0: View?) {

        when(p0?.id){
            R.id.tvgpoints ->
            {
                startActivity(Intent(activity, GPoint_Activity::class.java))
            }
            R.id.tv_howitwork ->
            {
                startActivity(Intent(activity, Howitworks_Activity::class.java))
            }
           R.id.tvfaq ->
            {
                startActivity(Intent(activity, Faq_Activity::class.java))
            }
           R.id.tv_contactus ->
            {
                startActivity(Intent(activity, ContactActivity::class.java))
            }
             R.id.tv_products ->
            {
                startActivity(Intent(activity, Product_Activity::class.java))
            }
             R.id.tv_share_apps ->
            {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    "Hey check out my app at: https://www.dropbox.com/sh/1oupbofykxwgx3i/AABZvPbkTsRReveLpFgF1w8Na?dl=0"
                )
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
            }
           R.id.tv_register ->
            {
                startActivity(Intent(activity, Register_Activity::class.java))
            }
            R.id.tvwishlist ->
            {
                fragmentManager!!.beginTransaction().replace(R.id.framelay, WishList_Fragment()).commit()
                activity!!.bottom_navigation.selectedItemId = R.id.menu_Wishlist
            }
             R.id.tvtickets ->
            {
                fragmentManager!!.beginTransaction().replace(R.id.framelay, Ticket_Fragment()).commit()
                activity!!.bottom_navigation.selectedItemId = R.id.menu_Tickets
            }
            R.id.tvlogout ->
            {
               prefrences.Clear()
                onResume()
            }
             R.id.tvchangepass ->
            {
                startActivity(Intent(activity, ChangePassword_Activity::class.java))
            }
            R.id.tv_language ->
            {
//                val myRoundedBottomSheet = Language_BottomSheetDialog()
//                myRoundedBottomSheet.show(fragmentManager!!, myRoundedBottomSheet.tag)

            }
             R.id.tv_forgotpass ->
            {
                val myRoundedBottomSheet = ForgotPassword_BottomSheetDialog()
                myRoundedBottomSheet.show(fragmentManager!!, myRoundedBottomSheet.tag)

            }
              R.id.tv_notification ->
            {
                val myRoundedBottomSheet = Notification_BottomSheetDialog()
                myRoundedBottomSheet.show(fragmentManager!!, myRoundedBottomSheet.tag)

            }
               R.id.tv_currency ->
            {
                val myRoundedBottomSheet = Currency_BottomSheetDialog()
                myRoundedBottomSheet.show(fragmentManager!!, myRoundedBottomSheet.tag)

            }
            R.id.tv_signin ->
            {
                checkvalidation()
            }
            R.id.ivuser ->
            {

                CropImage.activity()
                    .start(activity!!, this);
            }
   R.id.tvpersonaldetails ->
            {

             startActivity( Intent(activity,Profile_Activity::class.java))

            }


        }
     }



    override fun onResume() {
        super.onResume()
        if (prefrences.getlogin()!!){
            view!!.llsignin.visibility =View.GONE

            view!!.cvuser.visibility =View.VISIBLE

            view!!.tvusername.setText(prefrences.getname()+" "+prefrences.getlname())

            Glide.with(activity!!).load(Common.Image_Loading_Url(prefrences.getimage()))
                .error(R.drawable.userimage).into(view!!.ivuser)

            view!!.tvemail.setText("Using email "+prefrences.getemail())
        }else{
            view!!.llsignin.visibility =View.VISIBLE
            view!!.cvuser.visibility =View.GONE

        }


        if (prefrences.getlogin()!!){
            view!!.rl_notification.visibility = View.VISIBLE
            view!!.view_notification.visibility = View.VISIBLE
            view!!.tvchangepass.visibility = View.VISIBLE
            view!!.view_contactus.visibility = View.VISIBLE
            view!!.tv_contactus.visibility = View.VISIBLE
        }else{
            view!!.rl_notification.visibility = View.GONE
            view!!.view_notification.visibility = View.GONE
            view!!.tvchangepass.visibility = View.GONE
            view!!.view_contactus.visibility = View.GONE
            view!!.tv_contactus.visibility = View.GONE
        }

        view!!.tv_currency_name.setText(prefrences1.getcurrency())
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === RESULT_OK) {


                outPutfileUri= result.uri
                updateprofileimage()


            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    private fun checkvalidation(){
        Common.hideSoftKeyboardinfragment(activity!!)
        if ( Common.isEmailValid(view!!.et_email.text.toString().trim())){
            if (view!!.et_password.text.toString().length>5){
                login()
            }else{
                Common.Snakbar("Please enter correct password",view!!);

            }
        }
        else {
            Common.Snakbar("Please enter valid email",view!!);

        }
    }


    fun login(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object :StringRequest(Request.Method.POST,baseurl+"login", Response.Listener<String>
        {
                response ->

            Common.loggg("res",response);

            val obj= JSONObject(response)
            if (obj.getString("status").equals("true")){
                prefrences.setuserid(Common.ParseString(obj.getJSONObject("data"),Id))
                prefrences.setname(Common.ParseString(obj.getJSONObject("data"),F_name))
                prefrences.setlname(Common.ParseString(obj.getJSONObject("data"),L_name))
                prefrences.setimage(Common.ParseString(obj.getJSONObject("data"),Images))

                prefrences.setemail(Common.ParseString(obj.getJSONObject("data"),Email))
                prefrences.setmobile(Common.ParseString(obj.getJSONObject("data"),Phone))
                prefrences.setgender(Common.ParseString(obj.getJSONObject("data"),Gender))
                prefrences.setnotify_status(Common.ParseString(obj.getJSONObject("data"),"notify_status"))
                prefrences.setpromo(Common.ParseString(obj.getJSONObject("data"),"promo"))
                prefrences.setlogin(true)

                onResume()

            }else if (obj.getString("status").equals("false")){
                Common.Snakbar(obj.getString("message") ,view!!);
            }
            pd.cancel()

        },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["email"] = view!!.et_email.text.toString().trim()
                param["password"]=view!!.et_password.text.toString()
                val userSharedPreferences=UserSharedPrefrences.getInstancee(activity!!)
                param["player_id"]=userSharedPreferences.getplayerid()
                param["device_id"]=userSharedPreferences.getdeviceid()
                param["version_code"]=(context!!.getPackageManager().getPackageInfo(context!!.getPackageName(), 0).versionName)+"";
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }


     var outPutfileUri: Uri? = null

    fun updateprofileimage(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object : SimpleMultiPartRequest(
            Request.Method.POST,baseurl+"update_profile", Response.Listener<String>
            {
                    response ->

                Common.loggg("update_profile",response);
                try {


                    val obj= JSONObject(response)
                    if (obj.getString("status").equals("true")){

                              prefrences.setimage(Common.ParseString(obj.getJSONObject("data"),Images))

                        Glide.with(activity!!).load(Common.Image_Loading_Url(prefrences.getimage())).into(view!!.ivuser)


                        Toast.makeText(activity!!,"Profile image has been updated",Toast.LENGTH_LONG).show()
                    }else if (obj.getString("status").equals("false")){
                        Common.Snakbar(obj.getString("message") ,view!!);
                    }
                    pd.cancel()
                }catch (e : Exception){
                    Common.loggg("exce", e.toString())
                }

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {}
        if (outPutfileUri!=null) {
            stringRequest.addFile("image",outPutfileUri.toString().replace("file:", "") + "")

        }
            stringRequest.addStringParam(User_id, prefrences.getuserid())
          stringRequest.addStringParam(F_name, prefrences.getname())
          stringRequest.addStringParam(L_name, prefrences.getlname())
          stringRequest.addStringParam(Gender, prefrences.getgender())

        Common.loggg("params1", stringRequest.getBodyContentType() + "")


        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }


}