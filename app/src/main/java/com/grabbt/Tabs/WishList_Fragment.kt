package com.grabbt.Tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.WishItems_Adapter
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.EndlessRecyclerViewScrollListener
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.WishList
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.wishlist_fragment.*
import kotlinx.android.synthetic.main.wishlist_fragment.view.*
import org.json.JSONObject
import java.util.HashMap

class WishList_Fragment : Fragment() ,SwipeRefreshLayout.OnRefreshListener ,ParamKeys{

    lateinit var recyclerview: RecyclerView
    private lateinit var linearLayoutManager:LinearLayoutManager
     private lateinit var recyclerViewScrollListener:EndlessRecyclerViewScrollListener

    val list: ArrayList<WishList> = ArrayList()
    lateinit var prefrences : UserSharedPrefrences
    var index : Int = 1
    lateinit var view1: View



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.wishlist_fragment, container, false);
        view1 = view;
        initil(view)
        return view
    }

    private fun initil(view: View) {
        prefrences = UserSharedPrefrences.getInstance(activity!!)
        recyclerview=view.recycler_closing;
        linearLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = linearLayoutManager
        recyclerview.adapter= WishItems_Adapter(context,list,view!!. tvemptyview,recyclerview)
        recyclerview.isNestedScrollingEnabled = false


        view. swiprefres.setColorSchemeColors(
            activity!!.resources.getColor(R.color.colorPrimary),
            activity!!.resources.getColor(R.color.colorPrimary), activity!!.resources.getColor(R.color.colorPrimary)
        )
        view.swiprefres.setOnRefreshListener(this)

        if (prefrences.getlogin()!!) {
            get_wishlist(view1)
        }else{
            checklist(view)
        }

        recyclerViewScrollListener =
            object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {


                    if (prefrences.getlogin()!!) {
                        index++
                        get_wishlist(view1)
                    }
                }
            }

        recyclerview.addOnScrollListener(recyclerViewScrollListener)
    }


    override fun onRefresh() {
        index = 1
        list.clear()
        get_wishlist(view1 )
        view!!.   swiprefres.isRefreshing = false


    }


    fun get_wishlist(view: View){


        val pd= DelayedProgressDialog()
        if (index==0) {
            pd.show(fragmentManager!!, "")
        }
        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"get_wishlist", Response.Listener<String>
            {
                    response ->

                Common.loggg("get_wishlist",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONObject("data").getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                        val jsobj=jsonArray.getJSONObject(i)

                        list.add( WishList(
                            Common.ParseString(jsobj, "cart_id"),
                            Common.ParseString(jsobj, "id"),
                            Common.ParseString(jsobj, "product_title"),
                            Common.ParseString(jsobj, "product_disc"),
                            Common.ParseString(jsobj, "product_spec"),
                            Common.ParseString(jsobj, "product_image"),
                            Common.ParseString(jsobj, "price"),
                            Common.ParseString(jsobj, "quantity"),
                            Common.ParseString(jsobj, "prize_title"),
                            Common.ParseString(jsobj, "prize_spec"),
                            Common.ParseString(jsobj, "prize_image"),
                            Common.ParseString(jsobj, "prize_video"),"0",
                            Common.ParseString(jsobj, "prize_disc")  ,
                            Common.ParseString(jsobj, "order_count")))
                    }

                    recyclerview.adapter!!.notifyDataSetChanged()

                }
                else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }
                pd.cancel()
                checklist(view)
            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()
                 param["page"] =index.toString()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

    fun checklist(view: View){

        if (list!=null){
        if (list.size>0){
            view. tvemptyview.visibility = View.GONE
           view. recycler_closing.visibility = View.VISIBLE
        }else{
            view.  recycler_closing.visibility = View.GONE
            view. tvemptyview.visibility = View.VISIBLE
        }}
    }

}