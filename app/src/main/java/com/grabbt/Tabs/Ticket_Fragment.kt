package com.grabbt.Tabs

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.Tickettems_Adapter
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.EndlessRecyclerViewScrollListener
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.Tickets
import com.grabbt.Models.WishList
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
//import com.paytabs.paytabs_sdk.payment.ui.activities.PayTabActivity
//import com.paytabs.paytabs_sdk.utils.PaymentParams
import kotlinx.android.synthetic.main.ticketlist_fragment.view.*
import kotlinx.android.synthetic.main.ticketlist_fragment.view.recycler_closing
import kotlinx.android.synthetic.main.ticketlist_fragment.view.swiprefres
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class Ticket_Fragment : Fragment() , SwipeRefreshLayout.OnRefreshListener , ParamKeys {

    lateinit var recyclerview: RecyclerView
    private lateinit var linearLayoutManager:LinearLayoutManager

    private lateinit var recyclerViewScrollListener: EndlessRecyclerViewScrollListener

    val list: ArrayList<Tickets> = ArrayList()
    lateinit var prefrences : UserSharedPrefrences
    var index : Int = 1;
    lateinit var view1: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.ticketlist_fragment, container, false);
        view1=view
        initil(view)
        return view
    }

    private fun initil(view: View) {
        prefrences= UserSharedPrefrences.getInstance(activity!!)

        recyclerview=view.recycler_closing;
        linearLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = linearLayoutManager
        recyclerview.adapter= Tickettems_Adapter(context,list)
        recyclerview.isNestedScrollingEnabled = false



        view. swiprefres.setColorSchemeColors(
            activity!!.resources.getColor(R.color.colorPrimary),
            activity!!.resources.getColor(R.color.colorPrimary), activity!!.resources.getColor(R.color.colorPrimary)
        )
        view.swiprefres.setOnRefreshListener(this)

        if (prefrences.getlogin()!!) {
            get_tickets()
        }
        else{
            checklist()
        }

//        recyclerViewScrollListener =
//            object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
//                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
//
//
//                    if (prefrences.getlogin()!!) {
////                        index++
////                        get_wishlist()
//                    }
//                }
//            }
//
//        recyclerview.addOnScrollListener(recyclerViewScrollListener)

    }

    override fun onRefresh() {

        list.clear()
        get_tickets()
        view!!.   swiprefres.isRefreshing = false


    }

    fun get_tickets(){


        val pd= DelayedProgressDialog()
        if (index==0) {
            pd.show(fragmentManager!!, "")
        }
        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"view_tickets", Response.Listener<String>
            {
                    response ->

                Common.loggg("view_tickets",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONObject("data").getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                        val jsobj=jsonArray.getJSONObject(i)

                            loggg("data1",jsobj.getString("tickets"))

                        for (k in 0..(jsobj.getJSONArray("compaigns")).length()-1) {

                                val jsonObject  = jsobj.getJSONArray("compaigns").getJSONObject(k)

                            for (j in 0..(jsonObject.getJSONArray("tickets")).length()-1) {


//
                            list.add( Tickets(
                                Common.ParseString(jsonObject, "order_id"),
                                Common.ParseString(jsonObject, "id"),
                                Common.ParseString(jsonObject, "product_title"),
                                Common.ParseString(jsonObject, "product_disc"),
                                Common.ParseString(jsonObject, "product_spec"),
                                Common.ParseString(jsonObject, "product_image"),
                                Common.ParseString(jsonObject, "price"),
                                Common.ParseString(jsonObject, "quantity"),
                                Common.ParseString(jsonObject, "prize_title"),
                                Common.ParseString(jsonObject, "prize_spec"),
                                Common.ParseString(jsonObject, "prize_image")
                                ,
                                Common.ParseString(jsonObject, "prize_video"),
                                Common.ParseString(jsonObject, "prize_disc")  ,
                                jsonObject.getJSONArray("tickets").getString(j),
                                Common.ParseString(jsonObject,"order_count"),
                                jsobj.getString("timestamp")
                            ))
                        }
                        }


                    }

                    recyclerview.adapter!!.notifyDataSetChanged()

                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }
                pd.cancel()
                checklist()
            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()
                param["page"] =index.toString()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

    fun checklist(){

        if (list.size>0){
            view1. tvemptyview.visibility = View.GONE
            recyclerview.visibility = View.VISIBLE
        }else{
            recyclerview.visibility = View.GONE
            view1. tvemptyview.visibility = View.VISIBLE
        }
    }


}