package com.grabbt.Tabs

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.*
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.ExoPlayerActivity
import com.grabbt.Models.HomeProducts
import com.grabbt.R
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.ParseString
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.home_fragment.view.*
import kotlinx.android.synthetic.main.home_fragment.view.worm_dots_indicator
import org.json.JSONObject
import java.util.HashMap

class Home_Fragment : Fragment(),View.OnClickListener ,ParamKeys {

    lateinit var prefrences: UserSharedPrefrences

    lateinit var recyclerview: RecyclerView
    lateinit var viewpager: ViewPager
    lateinit var recycler_closing: RecyclerView
    private lateinit var linearLayoutManager:LinearLayoutManager
    private lateinit var cloasing_linearLayoutManager:LinearLayoutManager
    val list: ArrayList<HomeProducts> = ArrayList()
    val list_campaigns: ArrayList<HomeProducts> = ArrayList()
    val listimage:ArrayList<String> = ArrayList()
    lateinit var view1: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        view1= inflater.inflate(R.layout.home_fragment, container, false);
        initil()
        return view1
    }



    private fun initil() {

        prefrences =UserSharedPrefrences.getInstance(activity!!)

        recyclerview=view1.recycler_explore;
        recyclerview.isNestedScrollingEnabled = false
        linearLayoutManager = LinearLayoutManager(context)
        recyclerview.layoutManager = linearLayoutManager

        viewpager=view1.viewpager

        fetchlist()

        recycler_closing=view1.recycler_closing;
        cloasing_linearLayoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        recycler_closing.layoutManager = cloasing_linearLayoutManager
        campain_list()


//        fetchlist()


    }

    override fun onClick(p0: View?)
    {
        when(p0?.id)
        {
        }

    }



    fun fetchlist(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"home", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("res",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1))
                    {
                        val jsobj=jsonArray.getJSONObject(i)
                        listimage.add(ParseString(jsobj,"prize_image"))
                        list.add( HomeProducts(ParseString(jsobj,"id"),ParseString(jsobj,"product_title"),
                            ParseString(jsobj,"product_disc"),
                            ParseString(jsobj,"product_spec"),ParseString(jsobj,"product_image"),
                            ParseString(jsobj,"price"),ParseString(jsobj,"quantity"),
                            ParseString(jsobj,"prize_title"),ParseString(jsobj,"prize_spec"),
                            ParseString(jsobj,"prize_image")
                                   , ParseString(jsobj,"prize_video"),ParseString(jsobj,"cart_count")
                        ,
                            ParseString(jsobj,"prize_disc"), ParseString(jsobj,"wishlist_status").toBoolean(),
                            ParseString(jsobj,"draw_date")
                            , ParseString(jsobj,"order_count")  ))
                    }

                    recyclerview.adapter= HomeMainItems_Adapter(context,list,this)
                    viewpager.adapter= Image_Viewpage_Adapter(activity!!,listimage)
                    view1.worm_dots_indicator.setViewPager(viewpager)
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }

    fun campain_list(){


        val pd= DelayedProgressDialog()

        pd.show(fragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"closing_compaigns", Response.Listener<String>
            {
                response ->
                pd.cancel()
                loggg("closing_compaigns",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){

                    val jsonArray = obj.getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                    val jsobj=jsonArray.getJSONObject(i)

                        list_campaigns.add( HomeProducts(ParseString(jsobj,"id"),ParseString(jsobj,"product_title"),
                            ParseString(jsobj,"product_disc"),
                            ParseString(jsobj,"product_spec"),ParseString(jsobj,"product_image"),
                            ParseString(jsobj,"price"),ParseString(jsobj,"quantity"),
                            ParseString(jsobj,"prize_title"),ParseString(jsobj,"prize_spec"),
                            ParseString(jsobj,"prize_image")
                                   , ParseString(jsobj,"prize_video"),ParseString(jsobj,"cart_count"),
                            ParseString(jsobj,"prize_disc"), ParseString(jsobj,"wishlist_status").toBoolean(),
                            ParseString(jsobj,"draw_date")
                            , ParseString(jsobj,"order_count")  ))
                    }

                    recycler_closing.adapter= CloasingItems_Adapter(context,list_campaigns)
                    loggg("size1",list_campaigns.size.toString())
                    view1.tvcampain.visibility =View.VISIBLE
                    recycler_closing.visibility =View.VISIBLE


                }else if (obj.getString("status").equals("false") && obj.getString("message").equals("Nothing Found!!")){
                    view1.tvcampain.visibility =View.INVISIBLE
                    recycler_closing.visibility =View.GONE

                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(activity!!).addToRequestQueue(stringRequest)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        loggg("onActivityresult",data.toString()+","+requestCode)
        if (data!=null && resultCode== Activity.RESULT_OK){

            list.get(data.getIntExtra("position",0)).isWishlist =data.getBooleanExtra("isWishlist",false)
            list.get(data.getIntExtra("position",0)).addedquantity =data.getStringExtra("addedquantity")
            recyclerview.adapter!!.notifyItemChanged(data.getIntExtra("position",0))
        }
    }
}