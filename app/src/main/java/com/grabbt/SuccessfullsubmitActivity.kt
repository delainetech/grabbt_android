package com.grabbt

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.Adapters.Tickettems_Adapter
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.HomeProducts
import com.grabbt.Models.Tickets
import com.grabbt.Utils.Common
import com.grabbt.Utils.Common.Companion.loggg
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.successfull_checkout_screen.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SuccessfullsubmitActivity : AppCompatActivity(),ParamKeys,View.OnClickListener  {

    private lateinit var linearLayoutManager:LinearLayoutManager

    val list: ArrayList<Tickets> = ArrayList()
    lateinit var prefrences : UserSharedPrefrences
    var orderid:String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.successfull_checkout_screen)

        checkvie.check()
        prefrences= UserSharedPrefrences.getInstance(this)

        orderid=  intent.getStringExtra("order_id")
        val list: ArrayList<Tickets> = ArrayList()

        linearLayoutManager = LinearLayoutManager(this)
        recycler_explore.layoutManager = linearLayoutManager
        recycler_explore.adapter= Tickettems_Adapter(this,list)
        recycler_explore.isNestedScrollingEnabled = false

        if (prefrences.getlogin()!!) {
            get_tickets(recycler_explore)
        }
        ivcheck.setOnClickListener(this)
    }


    fun get_tickets(view: View){


        val pd= DelayedProgressDialog()

            pd.show(supportFragmentManager!!, "")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"view_tickets", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("view_tickets",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){
                    val jsonArray = obj.getJSONObject("data").getJSONArray("data")

                    for (i in 0..(jsonArray.length()-1)){
                        val jsobj=jsonArray.getJSONObject(i)

                        loggg("data1",jsobj.getString("tickets"))

                        for (k in 0..(jsobj.getJSONArray("compaigns")).length()-1) {

                            val jsonObject  = jsobj.getJSONArray("compaigns").getJSONObject(k)

                            for (j in 0..(jsonObject.getJSONArray("tickets")).length()-1) {


//
                                list.add( Tickets(
                                    Common.ParseString(jsonObject, "order_id"),
                                    Common.ParseString(jsonObject, "id"),
                                    Common.ParseString(jsonObject, "product_title"),
                                    Common.ParseString(jsonObject, "product_disc"),
                                    Common.ParseString(jsonObject, "product_spec"),
                                    Common.ParseString(jsonObject, "product_image"),
                                    Common.ParseString(jsonObject, "price"),
                                    Common.ParseString(jsonObject, "quantity"),
                                    Common.ParseString(jsonObject, "prize_title"),
                                    Common.ParseString(jsonObject, "prize_spec"),
                                    Common.ParseString(jsonObject, "prize_image")
                                    ,
                                    Common.ParseString(jsonObject, "prize_video"),
                                    Common.ParseString(jsonObject, "prize_disc")  ,
                                    jsonObject.getJSONArray("tickets").getString(j),
                                    Common.ParseString(jsonObject,"order_count"),
                                    jsobj.getString("timestamp")
                                ))
                            }
                        }


                    }


                    loggg("list",list.size.toString())
                    recycler_explore.adapter= Tickettems_Adapter(this,list)
                    recycler_explore.adapter!!.notifyDataSetChanged()
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()
                param["order_id"] =orderid

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }


    override fun onClick(p0: View?) {
        onBackPressed()
    }


    override fun onBackPressed() {
        val intent1 = Intent(this, HomeActivity::class.java)
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent1)
        finish()
    }
}