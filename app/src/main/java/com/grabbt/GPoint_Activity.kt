package com.grabbt

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Models.HomeProducts
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.gpoints_activity.*
import kotlinx.android.synthetic.main.home_fragment.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class GPoint_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys {

    lateinit var prefrences: UserSharedPrefrences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.gpoints_activity)

        prefrences =UserSharedPrefrences.getInstance(this)
        ivback.setOnClickListener(this)
        tv_date.setText(date())
        gpoints()

    }
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.ivback ->{
                finish()
            }


        }

    }


    private fun date(): String {
        val c = Calendar.getInstance()
        println("Current time => " + c.time)

        val df = SimpleDateFormat("dd MMM", Locale.US)

        return df.format(c.time)
    }


    fun gpoints(){


        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"g_points", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("g_points",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){
                    tv_available_points.setText(obj.getString("earn_throght_purchase"))
                    tv_earned.setText(obj.getString("earn_throght_purchase"))
                    tv_earn.setText(obj.getString("earn_throght_purchase"))
                    tv_coins.setText(obj.getString("earn_throght_purchase"))
                    tv_spend.setText(obj.getString("total_spend"))

                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,tv_balance);
                }


            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),tv_balance);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["user_id"] = prefrences.getuserid()

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }


}