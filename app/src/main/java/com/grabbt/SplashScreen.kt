package com.grabbt

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import com.grabbt.Utils.UserSharedPrefrences

class SplashScreen : AppCompatActivity() {

    private val SPLASH_TIME_OUT:Long=3000 // 3 sec
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val userSharedPreferences=UserSharedPrefrences.getInstancee(this)

        val androidId = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        )
        userSharedPreferences.setdeviceid(androidId)
        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity


            startActivity(Intent(this,HomeActivity::class.java))

            // close this activity
            finish()
        }, SPLASH_TIME_OUT)
    }
}
