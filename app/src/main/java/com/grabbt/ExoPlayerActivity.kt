package com.grabbt

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.videoview.*
import java.security.AccessController.getContext

class ExoPlayerActivity : AppCompatActivity() ,View.OnClickListener {

    private val BANDWIDTH_METER = DefaultBandwidthMeter()

    private var player: SimpleExoPlayer? = null
    private var playerView: SimpleExoPlayerView? = null

    private var playbackPosition: Long = 0
    private var currentWindow: Int = 0
    private var playWhenReady = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.videoview)

        initInstances()

    }

    override fun onClick(p0: View?) {
    }


    private fun initInstances() {
            playerView = findViewById(R.id.video_view)
            playerView!!.setUseController(false)
                    ivspeaker.setOnClickListener(this)

                }


    public override fun onStart() {
                    super.onStart()
                    if (Util.SDK_INT > 23)
                    {
                    initializePlayer()
                    }
             }


                public override fun onResume() {
                    super.onResume()
                    if (Util.SDK_INT <= 23 || player == null)
                    {
                    initializePlayer()
                    }
              }


                public override fun onPause() {
                    super.onPause()
                    if (Util.SDK_INT <= 23)
                    {
                    releasePlayer()
                    }
                }



                public override fun onStop() {
                    super.onStop()
                    if (Util.SDK_INT > 23)
                    {
                    releasePlayer()
                    }
              }



            private fun initializePlayer() {
                if (player == null)
                {
                player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(this),
                DefaultTrackSelector(),
                DefaultLoadControl()
            )
            player!!.setVolume(0f)
            playerView!!.setPlayer(player)
            player!!.setPlayWhenReady(playWhenReady)
            player!!.seekTo(currentWindow, playbackPosition)
            }
            val mediaSource = buildMediaSource(Uri.parse("https://gcs-vimeo.akamaized.net/exp=1568824363~acl=%2A%2F1475358109.mp4%2A~hmac=7edb69e05bdf4a2c0751f5f2143871a6751f8a6e44d1ce0de2eb28e7b4d763ba/vimeo-prod-skyfire-std-us/01/2015/14/360076954/1475358109.mp4"))
            player!!.prepare(mediaSource, true, false)
            }

                private fun releasePlayer() {
            if (player != null)
            {
            playbackPosition = player!!.getCurrentPosition()
            currentWindow = player!!.getCurrentWindowIndex()
            playWhenReady = player!!.getPlayWhenReady()
            player!!.release()
            player = null
            }
            }

                private fun buildMediaSource(uri: Uri): MediaSource {

            val userAgent = "exoplayer-codelab"

            if (uri.lastPathSegment!!.contains("mp3") || uri.lastPathSegment!!.contains("mp4"))
            {
            return ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
            .createMediaSource(uri)
            }
            else if (uri.lastPathSegment!!.contains("m3u8"))
            {
            return HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
            .createMediaSource(uri)
            }
            else
            {
            val dashChunkSourceFactory = DefaultDashChunkSource.Factory(
            DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)
            )
            val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
            return DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory).createMediaSource(uri)
            }
            }

                @SuppressLint("InlinedApi")
            private fun hideSystemUiFullScreen() {
            playerView!!.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            )
            }

                @SuppressLint("InlinedApi")
            private fun hideSystemUi() {
            playerView!!.setSystemUiVisibility((View.SYSTEM_UI_FLAG_LOW_PROFILE
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION))
            }


                override fun onConfigurationChanged(newConfig: Configuration) {
                    super.onConfigurationChanged(newConfig)

                    val currentOrientation = resources.configuration.orientation
                    if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE)
                    {
                    hideSystemUiFullScreen()
                    }
                    else
                    {
                    hideSystemUi()
                    }
            }


}