package com.grabbt

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Dialog.Gender_BottomSheetDialog
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.signup_activity.*
import org.json.JSONObject
import java.util.HashMap
import com.heetch.countrypicker.CountryPickerCallbacks
import com.heetch.countrypicker.CountryPickerDialog
import com.grabbt.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.forgotpass_activity.view.*
import kotlinx.android.synthetic.main.verifyotp_activity.*
import java.lang.Exception


class OTP_Verification_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys{

    lateinit var mcoxt: Context;
    var otp : String =""
    var iduser : String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.verifyotp_activity)
        mcoxt=this;

        tv_otpverification.setOnClickListener(this)
        et_otp.setOnClickListener(this)
        ivback.setOnClickListener(this)
        tv_resend.setOnClickListener(this)

        otp=     intent.getStringExtra("otp")
        iduser=   intent.getStringExtra("id")
          intent.getStringExtra("email")


    }




    override fun onClick(p0: View?) {
        when (p0!!.id){

         R.id.tv_otpverification -> {
                Common.hideSoftKeyboard(this)
             if (et_otp.text.toString().equals(otp)){

                 val intent = Intent(this,ResetPassword_Activity::class.java)
                 intent.putExtra("id",iduser)
                 startActivity(intent)
                finish()
             }else{
                 Common.Snakbar("You have entred incorrect otp",et_otp)
             }
           }

         R.id.ivback ->{

             finish()
         }
       R.id.tv_resend ->{

           resendotp(et_otp,this)

             }


    }
}




    fun resendotp(view: View,context: Context){


        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"forgot_password", Response.Listener<String>
            {
                    response ->

               loggg("forgot_password",response);

                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")) {

                    otp=obj.getJSONObject("data").getString("otp")


                    Toast.makeText(context,"OTP has been re-sent", Toast.LENGTH_LONG).show()

                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }
                pd.cancel()

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["email"] = intent.getStringExtra("email")

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

}
