package com.grabbt.CustomClass

import android.view.Gravity
import android.opengl.ETC1.getWidth
import android.os.Build
import android.opengl.ETC1.getHeight
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import android.graphics.drawable.shapes.RectShape
import android.R
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.shapes.Shape
import android.util.AttributeSet
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import java.util.*


class CustomProgress : TextView{

    private val SHAPE_RECTANGLE = 0
    private val SHAPE_ROUNDED_RECTANGLE = 1
    private val DEFAULT_TEXT_MARGIN = 10

    private var progressDrawable: ShapeDrawable? = null
    private val textView: TextView? = null
//    lateinit var width = 0
//    private var maxWidth = 0
//    private val maxHeight = 0
    private var progressColor: Int = 0
    private var progressBackgroundColor: Int = 0
    private var progressShape = SHAPE_RECTANGLE
    private var maximumPercentage = .0f
    private var cornerRadius = 25.0f
    private var showingPercentage = false
    private var speed = 20
    private var resetToZero = false

    //Constructor

    constructor(context: Context) : super(context) {
        setDefaultValue()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context,attrs) {
        setDefaultValue()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context,attrs,defStyle) {
        setDefaultValue()
    }

    private fun setDefaultValue() {
        // default color
        progressColor = Color.RED
        progressBackgroundColor = Color.WHITE
    }

    //View Lifecycle

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (changed) {
            initView()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        progressDrawable!!.setBounds(0, 0, width, this.getHeight())
        progressDrawable!!.draw(canvas)
        if (isShowingPercentage()) {
            this.setText(getCurrentPercentage().toString() + "%")
        }
        //if the we want to reset to 0
        if (resetToZero) {
            width = 0
            resetToZero = false
            invalidate()
        } else if (width < maxWidth) {
            width += this.speed
            invalidate()
        }
    }

    /**
     * Initialize the view before it will be drawn
     */
    fun initView() {
        var progressShapeDrawable: Shape? = null
        var backgroundProgressShapeDrawable: Shape? = null
        when (progressShape) {
            SHAPE_RECTANGLE -> {
                progressShapeDrawable = RectShape()
                backgroundProgressShapeDrawable = RectShape()
            }
            SHAPE_ROUNDED_RECTANGLE -> {
                val outerRadius = FloatArray(8)
                Arrays.fill(outerRadius, cornerRadius)
                progressShapeDrawable = RoundRectShape(outerRadius, null, null)
                backgroundProgressShapeDrawable = RoundRectShape(outerRadius, null, null)
            }
        }

        //Progress
        progressDrawable = ShapeDrawable(progressShapeDrawable)
        progressDrawable!!.paint.color = progressColor
        if (this.getText().length > 0 || isShowingPercentage()) {
            progressDrawable!!.alpha = 100
        }

        //Background
        val backgroundDrawable = ShapeDrawable(backgroundProgressShapeDrawable)
        backgroundDrawable.paint.color = progressBackgroundColor
        backgroundDrawable.setBounds(0, 0, this.getWidth(), this.getHeight())
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.setBackground(backgroundDrawable)
        } else {
            this.setBackgroundDrawable(backgroundDrawable)
        }

        this.maxWidth = (this.getWidth() * maximumPercentage).toInt()

        //Percentage
        if (isShowingPercentage()) {
            this.setTextSize(20F)
            this.setTextColor(Color.WHITE)
            this.setGravity(Gravity.CENTER)
        }
    }

    //Helper

    /**
     * Set the progress color
     * @param color
     */
    fun setProgressColor(color: Int) {
        this.progressColor = color
    }

    /**
     * Set the background color
     * @param color
     */
    fun setProgressBackgroundColor(color: Int) {
        this.progressBackgroundColor = color
    }

    /**
     * Reset the progress to 0
     */
    fun resetWidth() {
        width = 0
    }

    /**
     * Set the maximum percentage for the progress
     * @param percentage
     */
    fun setMaximumPercentage(percentage: Float) {
        this.maximumPercentage = percentage
    }

    /**
     * Get current percentage based on current width
     * @return
     */
    fun getCurrentPercentage(): Int {
        return Math.ceil((width / (maxWidth * 1.0f) * 100).toDouble()).toInt()
    }

    /**
     * Set the shape of custom progress to rectangle
     */
    fun useRectangleShape() {
        this.progressShape = SHAPE_RECTANGLE
    }

    /**
     * Set the shape of custom progress to rounded rectangle
     * @param cornerRadius radius of the corner
     */
    fun useRoundedRectangleShape(cornerRadius: Float) {
        this.progressShape = SHAPE_ROUNDED_RECTANGLE
        this.cornerRadius = cornerRadius
    }

    /**
     * If this returns true the custom progress
     * will show progress based on getCurrentPercentage()
     * @return true for showing percentage false for not showing anything
     */
    fun isShowingPercentage(): Boolean {
        return showingPercentage
    }

    /**
     * Set if the custom progress will show percentage or not
     * @param showingPercentage true for showing percentage false for not showing anything
     */
    fun setShowingPercentage(showingPercentage: Boolean) {
        this.showingPercentage = showingPercentage
    }

    /**
     * Set the speed of the movement of the progress
     * @param speed as an int it should range from [1,100]
     */
    fun setSpeed(speed: Int) {
        this.speed = speed
    }

    /**
     * call the function when you want to update view
     */
    fun updateView() {
        resetToZero = true
        initView()
        invalidate()
    }
}