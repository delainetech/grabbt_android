package com.grabbt.CustomClass.RounderBorderDialog

import android.content.Context
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.grabbt.R

open class RoundedBottomSheetDialog(context: Context) : BottomSheetDialog(context, R.style.BottomSheetDialogTheme)