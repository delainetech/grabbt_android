package com.grabbt

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Dialog.Gender_BottomSheetDialog
import com.grabbt.Dialog.Language_BottomSheetDialog
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import org.json.JSONObject
import java.util.HashMap
import com.heetch.countrypicker.Country
import com.heetch.countrypicker.CountryPickerCallbacks
import com.heetch.countrypicker.CountryPickerDialog
import com.grabbt.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.changepassword_activity.*
import kotlinx.android.synthetic.main.changepassword_activity.et_confirmpassword
import kotlinx.android.synthetic.main.changepassword_activity.et_password
import kotlinx.android.synthetic.main.signup_activity.*
import java.lang.Exception


class ChangePassword_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys{

    lateinit var mcoxt: Context;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.changepassword_activity)
        mcoxt=this;



      initial()

    }


    fun initial(){

        prefrences =UserSharedPrefrences.getInstance(this)



        tvupgrade.setOnClickListener(this)
        ivback.setOnClickListener(this)


    }


    override fun onClick(p0: View?) {
        when (p0!!.id){


           R.id.ivback -> {
               finish()
           }

         R.id.tvupgrade -> {
             check()
           }



        }

    }


    fun check(){
        if (etcurrentpass.text!!.length>0 && et_password.text!!.length>0 && et_confirmpassword.text!!.length>0
             ){

            if (et_password.text.toString().length>5){
            if (et_password.text.toString().equals(et_confirmpassword.text.toString())){

                Change_Password()
            }else{
                Common.Snakbar("New Password and Confirm Password does not match",view!!);
            }
            }else{
                Common.Snakbar("Password should be minimum of 6 digit",view!!);
            }

        }else{
            Common.Snakbar("Please fill all fileds",etcurrentpass);
        }
    }


    lateinit var prefrences: UserSharedPrefrences

    fun Change_Password(){


        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"change_password", Response.Listener<String>
            {
                    response ->

                Common.loggg("res",response);
                try {


                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){


                    pd.cancel()
                    finish()
                    Toast.makeText(this,"Password has been updated",Toast.LENGTH_LONG).show()
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,etcurrentpass);
                }

                }catch (e : Exception){
                loggg("exce",e.toString())
                }

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),etcurrentpass!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[User_id] = prefrences.getuserid()
                param["old_password"] = etcurrentpass.text.toString().trim()
                param["new_password"] = et_password.text.toString().trim()

                Common.loggg("params",param.toString())
                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }



}
