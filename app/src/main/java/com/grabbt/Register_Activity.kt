package com.grabbt

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.grabbt.CustomClass.DelayedProgressDialog
import com.grabbt.CustomClass.MySingleton
import com.grabbt.Dialog.Gender_BottomSheetDialog
import com.grabbt.Dialog.Language_BottomSheetDialog
import com.grabbt.Utils.Common
import com.grabbt.Utils.ParamKeys
import com.grabbt.Utils.UserSharedPrefrences
import kotlinx.android.synthetic.main.profile_fragment.view.*
import kotlinx.android.synthetic.main.profile_fragment.view.et_email
import kotlinx.android.synthetic.main.profile_fragment.view.et_password
import kotlinx.android.synthetic.main.signup_activity.*
import kotlinx.android.synthetic.main.signup_activity.view.*
import org.json.JSONObject
import java.util.HashMap
import com.heetch.countrypicker.Country
import com.heetch.countrypicker.CountryPickerCallbacks
import com.heetch.countrypicker.CountryPickerDialog
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.grabbt.Utils.Common.Companion.loggg
import java.lang.Exception


class Register_Activity : AppCompatActivity() ,View.OnClickListener ,ParamKeys{

    lateinit var mcoxt: Context;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup_activity)
        mcoxt=this;
         textclickable_login(tv_alreadyaccount);
        tv_alreadyaccount.setOnClickListener(this)
        tvsignin.setOnClickListener(this)
        et_gender.setOnClickListener(this)
        tv_register.setOnClickListener(this)
        tv_countrycode.setOnClickListener(this)

    }




    override fun onClick(p0: View?) {
        when (p0!!.id){
           R.id.et_gender -> {
               val myRoundedBottomSheet = Gender_BottomSheetDialog()
               myRoundedBottomSheet.show(supportFragmentManager!!, myRoundedBottomSheet.tag)
           }

           R.id.tv_alreadyaccount -> {
               finish()
           }
       R.id.tvsignin -> {
               finish()
           }

         R.id.tv_register -> {
             check()
           }

        R.id.tv_countrycode -> {
            val countryPicker = CountryPickerDialog(this,
                CountryPickerCallbacks { country, flagResId ->
                    tv_countrycode.setText("+"+country.dialingCode)

                })
            countryPicker.show()
                   }

        }

    }


    fun check(){
        if (et_firstname.text!!.trim().length>0 && et_lastname.text!!.trim().length>0 && et_gender.text!!.trim().length>0
            && et_email.text!!.length>0 && et_password.text!!.length>0&& et_confirmpassword.text!!.trim().length>0&&
            et_mobileno.text!!.trim().length>0){

            if ( Common.isEmailValid(et_email.text.toString().trim())){
                if (et_password.text.toString().length>5){

                    if (et_password.text.toString().equals(et_confirmpassword.text.toString())){
                        if (et_mobileno.text!!.trim().length>7) {
                            signup()
                        }else{
                            Common.Snakbar("Please enter correct mobile number",view!!);
                        }
                    }else{
                        Common.Snakbar("Password and Confirm Password does not match",view!!);
                    }

                }else{
                    Common.Snakbar("Password should be minimum of 6 digit",view!!);

                }
            }
            else {
                Common.Snakbar("Please enter valid email",view!!);

            }
        }else{
            Common.Snakbar("Please fill all fileds",view);
        }
    }


    private fun textclickable_login(tv_fixed: TextView) {
        val ss = SpannableString("Already have an Account? LOGIN")
        val Terms = object : ClickableSpan() {
            override fun onClick(textView: View) {
//                val intent1 = Intent(mcoxt, Login::class.java)
////                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
////                startActivity(intent1)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.color = mcoxt.getResources().getColor(R.color.colorPrimaryDark)
            }
        }

        ss.setSpan(Terms, 25, 30, 0)

        tv_fixed.movementMethod = LinkMovementMethod.getInstance()
        tv_fixed.setText(ss, TextView.BufferType.SPANNABLE)
        tv_fixed.isSelected = true
    }

    lateinit var prefrences: UserSharedPrefrences

    fun signup(){

        prefrences =UserSharedPrefrences.getInstance(this)
        val pd= DelayedProgressDialog()

        pd.show(supportFragmentManager!!,"")

        val stringRequest = object : StringRequest(
            Request.Method.POST,baseurl+"signup", Response.Listener<String>
            {
                    response ->
                pd.cancel()
                Common.loggg("res",response);
                try {


                val obj= JSONObject(response)
                if (obj.getString("status").equals("true")){
                    prefrences.setuserid(Common.ParseString(obj.getJSONObject("data"),Id))
                    prefrences.setname(Common.ParseString(obj.getJSONObject("data"),F_name))
                   prefrences.setlname(Common.ParseString(obj.getJSONObject("data"),L_name))
                   prefrences.setemail(Common.ParseString(obj.getJSONObject("data"),Email))
                    prefrences.setimage(Common.ParseString(obj.getJSONObject("data"),Images))

                    prefrences.setmobile(Common.ParseString(obj.getJSONObject("data"),Phone))
                   prefrences.setgender(Common.ParseString(obj.getJSONObject("data"),Gender))
                  prefrences.setnotify_status(Common.ParseString(obj.getJSONObject("data"),"notify_status"))
                    prefrences.setpromo(Common.ParseString(obj.getJSONObject("data"),"promo"))
                  prefrences.setlogin(true)

                    finish()
                }else if (obj.getString("status").equals("false")){
                    Common.Snakbar(obj.getString("message") ,view!!);
                }

                }catch (e : Exception){
                loggg("exce",e.toString())
                }

            },
            Response.ErrorListener {
                    error ->
                pd.cancel()
                Log.e("error","",error);
                Common.Snakbar( Common.volleyerror(error),view!!);

            })
        {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[F_name] = et_firstname.text.toString().trim()
                param[L_name] = et_lastname.text.toString().trim()
                param[Email] = et_email.text.toString().trim()
                param[Phone] = tv_countrycode.text.toString()+et_mobileno.text.toString().trim()
                param[Password]=et_password.text.toString()
                param[Gender]=et_gender.text.toString()
                param["promo"]=et_promocode.text.toString()
                val userSharedPreferences=UserSharedPrefrences.getInstancee(this@Register_Activity)
                param["player_id"]=userSharedPreferences.getplayerid()
                param["device_id"]=userSharedPreferences.getdeviceid()
                param["version_code"]=(this@Register_Activity.getPackageManager().getPackageInfo(this@Register_Activity.getPackageName(), 0).versionName)+"";

                return param
            }
        }

        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy


        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }



}
